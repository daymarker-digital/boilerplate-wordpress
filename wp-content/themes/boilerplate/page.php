<?php

  /**
   * Template Name: Page
   * Filename: page.php
   *
   * The main template for pages.
   *
   * @package WordPress
   * @subpackage boilerplate
   */

  get_header();

  if ( have_posts() ) {
    while ( have_posts() ) {

      the_post();

      the_content();

    }
  }

  get_footer();
