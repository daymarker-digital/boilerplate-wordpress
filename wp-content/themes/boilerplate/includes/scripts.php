<?php

  /**
   * Theme scripts.
   *
   * @package WordPress
   * @subpackage boilerplate
   */

  namespace DD\Project\Scripts;

  const SCRIPTS = [
    "lazysizes" => [
      "src" => "https://cdn.jsdelivr.net/npm/lazysizes@5.3.2/lazysizes.min.js",
      "load" => "async",
      "in_footer" => false,
    ],
    "gsap" => [
      "src" => "/js/vendor/gsap/gsap.min.js",
      "load" => "defer",
      "in_footer" => true,
    ],
    "gsap-scroll-trigger" => [
      "src" => "/js/vendor/gsap/ScrollTrigger.min.js",
      "load" => "defer",
      "in_footer" => true,
    ],
    "gsap-scroll-smoother" => [
      "src" => "/js/vendor/gsap/ScrollSmoother.min.js",
      "load" => "defer",
      "in_footer" => true,
    ],
    // "splide-js" => [
    //   "src" => "https://cdn.jsdelivr.net/npm/@splidejs/splide@4.1.4/dist/js/splide.min.js",
    //   "load" => "defer",
    //   "in_footer" => true,
    // ],
    // "lottie" => [
    //   "src" => "https://cdn.jsdelivr.net/npm/@lottiefiles/lottie-player@2.0.4/dist/lottie-player.min.js",
    //   "load" => "defer",
    //   "in_footer" => true,
    // ],
    // "lottie-interactivity" => [
    //   "src" => "https://cdn.jsdelivr.net/npm/@lottiefiles/lottie-interactivity@1.6.2/dist/lottie-interactivity.min.js",
    //   "load" => "defer",
    //   "in_footer" => true,
    // ],
    "main" => [
      "src" => "/assets/main.min.js",
      "load" => "defer",
      "in_footer" => true,
    ],
  ];

  const UNUSED_SCRIPTS = [
    "jquery",
    "comment-reply",
    "wp-embed"
  ];

  /**
   * Initialize collection of script functions.
   *
   * @return void
   */
  function init() {
    remove_unused_scripts();
    add_scripts();
  }

  /**
   * Register and enqueue scripts.
   *
   * @return void
   */
  function add_scripts() {
    foreach( SCRIPTS as $name => $script ) {

      global $wp_version;

      // Deconstruct data from $script
      $deps = $script["deps"] ?? [];
      $in_footer = $script["in_footer"] ?? true;
      $load = $script["load"] ?? "defer";
      $src = $script["src"] ?? "";
      $ver = $script["ver"] ?? $wp_version;
      $handle = "{$name}-{$load}";

      // Check if $src exists
      if ( !empty($src) ) {

        // If $src contains HTTP protocol
        if ( strpos( $src, "https://" ) === false ) {
          $path = DD_THEME_TEMPLATE_URL . $src;
          $ver = filemtime( DD_THEME_PATH . $src );
        } else {
          $path = $src;
        }

        // Conditionally load scripts based on "acf-blocks"
        switch ( $name ) {
          case "lottie":
          case "lottie-interactivity": {
            if ( has_block('acf/lottie') ) {
              wp_register_script($handle, $path, $deps, $ver, $in_footer);
              wp_enqueue_script($handle);
            }
            break;
          }
          default: {
            wp_register_script($handle, $path, $deps, $ver, $in_footer);
            wp_enqueue_script($handle);
            break;
          }
        }

        // If $script is the "main" script localize data
        if ( "main" === $name ) {
          wp_localize_script( $handle, "wp_data", [
            "root" => esc_url_raw(get_bloginfo("url")),
            "rootapiurl" => esc_url_raw(rest_url())
          ]);
        }

      }

    }
  }

  /**
   * Echo scripts within site <head> as preload links.
   *
   * @return void
   */
  function pre_load_scripts() {
    foreach( SCRIPTS as $name => $script ) {

      global $wp_version;

      // Deconstruct data from $script
      $src = $script["src"] ?? "";
      $ver = $script["ver"] ?? $wp_version;
      $html = "";

      // Check if $src exists
      if ( !empty($src) ) {

        // If $src contains HTTP protocol
        if ( strpos( $src, "https://" ) === false ) {
          $path = DD_THEME_TEMPLATE_URL . $src;
          $ver = filemtime( DD_THEME_PATH . $src );
        } else {
          $path = $src;
        }

        // Concatenate strings to build path with version
        $path_with_version = $path . "?ver=" . $ver;

        // Conditionally preload $path if page has block
        switch ( $name ) {
          case "lottie":
          case "lottie-interactivity": {
            if ( has_block('acf/lottie') ) {
              $html = "<link rel='preload' href='" . esc_url($path_with_version) . "' as='script' />";
            }
            break;
          }
          default: {
            $html = "<link rel='preload' href='" . esc_url($path_with_version) . "' as='script' />";
            break;
          }
        }

      }

      echo $html;

    }
  }

  /**
   * Dequeue and deregister unused scripts.
   *
   * @return void
   */
  function remove_unused_scripts() {
    foreach( UNUSED_SCRIPTS as $script ) {
      wp_dequeue_script($script);
      wp_deregister_script( $script );
    }
  }

