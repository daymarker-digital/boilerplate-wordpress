<?php

  /**
   * Theme ACF Blocks.
   *
   * @package WordPress
   * @subpackage boilerplate
   */

  namespace DD\Project\ACFBlocks;

  /**
   * Initialize functions.
   *
   * @return void
   */
  function init() {

    $n = function( $function ) {
      return __NAMESPACE__ . "\\$function";
    };

    add_action( "init", $n( "register_block_types" ) );
    add_action( "init", $n( "default_blocks_posts" ), 20 );
    add_action( "init", $n( "default_blocks_projects" ), 20 );

    add_filter( "allowed_block_types_all", $n("allowed_blocks"), 10, 2 );

  }

  /**
   * Get all blocks from the /blocks/ directory.
   *
   * @return mixed $blocks array of custom blocks
   */
  function get_blocks() {
    $blocks = [];
    if ( file_exists( DD_THEME_BLOCKS_PATH ) ) {
      $block_json_files = glob( DD_THEME_BLOCKS_PATH . '*/block.json' );
      foreach ( $block_json_files as $filename ) {
        $block_json_string = file_get_contents($filename);
        $block_json_data = json_decode($block_json_string, true);
        $block_json_data["directory_name"] = dirname($filename);
        $blocks[] = $block_json_data;
      }
    }
    return $blocks;
  }

  /**
   * Conditionally allow blocks to be available for use.
   *
   * @param mixed $allowed_block_types...
   * @param mixed $editor_context...
   * @return mixed $allowed_block_types...
   */
  function allowed_blocks( $allowed_block_types, $editor_context ) {

    if ( ! empty($editor_context->post) ) {

      $allowed_blocks = [];
      $blocks = get_blocks();

      foreach( $blocks as $block ) {
        $allowed_blocks[] = $block["name"];
      }

      return $allowed_blocks;

    }

    return $allowed_block_types;

  }

  /**
   * Register blocks for ACF integration.
   *
   * @return void
   */
  function register_block_types() {
    $blocks = get_blocks();
    foreach ( $blocks as $block ) {
      register_block_type($block["directory_name"]);
    }
  }

  /**
   * Preload new posts with specific block(s).
   *
   * @return void
   */
  function default_blocks_posts() {
    $post_type_object = get_post_type_object("post");
    if ( !empty($post_type_object) ) {
      $post_type_object->template = [
        [ "acf/post-intro" ],
        [ "acf/latest-articles" ]
      ];
    }
  }

  /**
   * Preload new projects with specific block(s).
   *
   * @return void
   */
  function default_blocks_projects() {
    $post_type_object = get_post_type_object("project");
    if ( !empty($post_type_object) ) {
      $post_type_object->template = [
        [ "acf/post-intro" ]
      ];
    }
  }
