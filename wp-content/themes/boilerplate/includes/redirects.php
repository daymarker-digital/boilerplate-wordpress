<?php

  /**
   * Theme redirects.
   *
   * @package WordPress
   * @subpackage boilerplate
   */

  namespace DD\Project\Redirects;

  /**
   * Gets list of redirects.
   */
  function get_redirects_list() {
    return [
      redirect_factory("redirect-me", 302, "/"),
    ];
  }

  /**
   * Redirect factory function.
   *
   * Builds an associative array for the init() function.
   *
   * @param string $slug of the relative path to be redirected.
   * @param int $type of redirect.
   * @param string $destination of the redirect.
   * @return array
   */
  function redirect_factory( $slug="/", $type=301, $destination="/" ) {
    return [
      "destination" => $destination,
      "slug" => $slug,
      "type" => $type,
    ];
  }

  /**
   * Initialize functions.
   *
   * Redirects relative paths from the get_redirects_list() function.
   *
   * @return void
   */
  function init() {

    global $wp;

    $current_slug = add_query_arg(array(), $wp->request);
    $redirects = get_redirects_list();

    foreach ( $redirects as $redirect ) {
      if ( $redirect["slug"] === $current_slug ) {
        $destination = get_site_url() .$redirect["destination"];
        wp_redirect($destination, $redirect["type"]);
        exit;
      }
    }

  }
