<?php

  /**
   * Theme custom post settings.
   *
   * @package WordPress
   * @subpackage boilerplate
   */

  namespace DD\Project\CustomPostTypes;

  const POST_TYPES = [
    "job" => [
      "name" => "Jobs",
      "name_singular" => "Job",
      "icon" => "dashicons-coffee",
      "archive_url" => false,
    ],
    "project" => [
      "name" => "Projects",
      "name_singular" => "Project",
      "icon" => "dashicons-portfolio",
      "archive_url" => "all-projects"
    ],
    "team-member" => [
      "name" => "Team",
      "name_singular" => "Team Member",
      "icon" => "dashicons-groups",
      "archive_url" => false,
    ]
  ];

  /**
   * Initialize custom post types with supported WordPress hooks.
   *
   * @return void
   */
  function init() {

    $n = function( $function ) {
      return __NAMESPACE__ . "\\$function";
    };

    add_action( "init", $n( "register_post_types" ) );

  }

  /**
   * Register custom post types.
   *
   * @return void
   */
  function register_post_types() {
    foreach ( POST_TYPES as $slug => $post_type ) {

      $archive_url = $post_type["archive_url"];
      $icon = $post_type["icon"];
      $name = $post_type["name"];
      $name_singular = $post_type["name_singular"];

      $labels = [
        "name"                => _x( $name, "Post Type General Name", "text_domain" ),
        "singular_name"       => _x( $name_singular, "Post Type Singular Name", "text_domain" ),
        "menu_name"           => __( $name, "text_domain" ),
        "name_admin_bar"      => __( $name_singular, "text_domain" ),
        "parent_item_colon"   => __( "Parent " . $name_singular . ":", "text_domain" ),
        "all_items"           => __( "All " . $name, "text_domain" ),
        "add_new_item"        => __( "Add New " . $name_singular, "text_domain" ),
        "add_new"             => __( "Add New", "text_domain" ),
        "new_item"            => __( "New " . $name_singular, "text_domain" ),
        "edit_item"           => __( "Edit " . $name_singular, "text_domain" ),
        "update_item"         => __( "Update " . $name_singular, "text_domain" ),
        "view_item"           => __( "View " . $name, "text_domain" ),
        "search_items"        => __( "Search " . $name, "text_domain" ),
        "not_found"           => __( $name . " Not found", "text_domain" ),
        "not_found_in_trash"  => __( $name . " Not found in Trash", "text_domain" ),
      ];

      $args = [
        "label"               => __( $name_singular, "text_domain" ),
        "description"         => __( "Posts for " . $name_singular, "text_domain" ),
        "labels"              => $labels,
        "supports"            => [ "title", "editor", "excerpt", "author", "thumbnail", "comments", "trackbacks", "revisions", "custom-fields", "page-attributes", "post-formats" ],
        "taxonomies"          => [ "category", "post_tag" ],
        "hierarchical"        => true,
        "public"              => true,
        "show_ui"             => true,
        "show_in_menu"        => true,
        "menu_position"       => 5,
        "show_in_admin_bar"   => true,
        "show_in_nav_menus"   => true,
        "can_export"          => true,
        "has_archive"         => $archive_url,
        "show_in_rest" 		    => true,
        "exclude_from_search" => false,
        "publicly_queryable"  => true,
        "capability_type"     => "page",
        "delete_with_user"    => false,
        "menu_icon"           => $icon,
        "rewrite"             => [ "slug" => $slug ]
      ];

      register_post_type( $slug, $args );

    }
  }
