<?php

  /**
   * Theme filters.
   *
   * @package WordPress
   * @subpackage boilerplate
   */

  namespace DD\Project\Filters;

  /**
   * Initialize collection of filter functions.
   *
   * @return void
   */
  function init() {

    $n = function( $function ) {
      return __NAMESPACE__ . "\\$function";
    };

    add_filter( "mce_buttons_2", $n("wysiwyg_formats_button") );
    add_filter( "tiny_mce_before_init", $n("wysiwyg_formats") );

  }

  /**
   * Addition of custom WYSIWYG format button to the MCE.
   *
   * @param mixed $buttons array of buttons
   * @return mixed $buttons array of updated buttons
   */
  function wysiwyg_formats_button( $buttons ) {
    array_unshift( $buttons, "styleselect" );
    return $buttons;
  }

  /**
   * Addition of custom WYSIWYG formats to the MCE.
   *
   * @param mixed $init_array array of style formats
   * @return mixed $init_array array of updated style formats
   */
  function wysiwyg_formats( $init_array ) {

    $style_formats = [
      array(
        "title" => "Fonts",
        "items" => array(
          wysiwyg_format_factory( "Neue Haas Grotesk Display Pro", "font-family--neue-haas-grotesk-display-pro", [] ),
          wysiwyg_format_factory( "Neue Haas Grotesk Text Pro", "font-family--neue-haas-grotesk-text-pro", [] ),
          wysiwyg_format_factory( "Tiempos Text", "font-family--tiempos-text", [] ),
        )
      ),
      array(
        "title" => "Font Size",
        "items" => array(
          wysiwyg_format_factory( "Font Size XL", "font-size--xl", [ "fontSize" => "160%" ] ),
          wysiwyg_format_factory( "Font Size LG", "font-size--lg", [ "fontSize" => "140%" ] ),
          wysiwyg_format_factory( "Font Size MD", "font-size--md", [ "fontSize" => "120%" ] ),
        )
      ),
      array(
        "title" => "Line Height",
        "items" => array(
          wysiwyg_format_factory( "Line Height 150", "", [ "display" => "block", "lineHeight" => "1.5" ] ),
          wysiwyg_format_factory( "Line Height 145", "", [ "display" => "block", "lineHeight" => "1.45" ] ),
          wysiwyg_format_factory( "Line Height 140", "", [ "display" => "block", "lineHeight" => "1.4" ] ),
          wysiwyg_format_factory( "Line Height 135", "", [ "display" => "block", "lineHeight" => "1.35" ] ),
          wysiwyg_format_factory( "Line Height 130", "", [ "display" => "block", "lineHeight" => "1.3" ] ),
          wysiwyg_format_factory( "Line Height 125", "", [ "display" => "block", "lineHeight" => "1.25" ] ),
          wysiwyg_format_factory( "Line Height 120", "", [ "display" => "block", "lineHeight" => "1.2" ] ),
          wysiwyg_format_factory( "Line Height 115", "", [ "display" => "block", "lineHeight" => "1.15" ] ),
          wysiwyg_format_factory( "Line Height 110", "", [ "display" => "block", "lineHeight" => "1.1" ] ),
          wysiwyg_format_factory( "Line Height 105", "", [ "display" => "block", "lineHeight" => "1.05" ] ),
          wysiwyg_format_factory( "Line Height 100", "", [ "display" => "block", "lineHeight" => "1.0" ] ),
        )
      ),
    ];

    $init_array["style_formats"] = json_encode( $style_formats );

    return $init_array;

  }

  /**
   * WYSIWYG format factory function.
   *
   * @param string $title of the style format
   * @param string $classes of the style format
   * @param mixed $styles of the style format
   * @return mixed
   */
  function wysiwyg_format_factory( $title = "", $classes = "", $styles = [] ) {
    return [
      "title" => $title,
      "inline" => "span",
      "classes" => $classes,
      "wrapper" => true,
      "styles" => $styles,
    ];
  }
