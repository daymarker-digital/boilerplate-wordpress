<?php

  /**
   * Theme fonts.
   *
   * @package WordPress
   * @subpackage boilerplate
   */

  namespace DD\Project\Fonts;

  const FONTS = [];

  /**
   * Echo fonts within site <head> as preload links.
   *
   * @return void
   */
  function pre_load_fonts() {
    foreach( FONTS as $font ) {
      $path = DD_THEME_ASSETS_URL . "fonts/{$font}.woff2";
      echo "<link rel='preload' href='" . esc_url($path) . "' as='font' type='font/woff2' crossorigin>";
    }
  }
