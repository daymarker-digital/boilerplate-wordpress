<?php

  /**
   * Theme core settings.
   *
   * @package WordPress
   * @subpackage boilerplate
   */

  namespace DD\Project\Core;

  use DD\Project\Fonts as ProjectFonts;
  use DD\Project\Redirects as ProjectRedirects;
  use DD\Project\Scripts as ProjectScripts;
  use DD\Project\Styles as ProjectStyles;

  /**
   * Initialize core theme function with supported WordPress actions.
   *
   * @return void
   */
  function init() {

    $n = function( $function ) {
      return __NAMESPACE__ . "\\$function";
    };

    add_action( "wp_enqueue_scripts", $n( "scripts" ), 0 );
    add_action( "wp_enqueue_scripts", $n( "styles" ), 0 );
    add_action( "script_loader_tag", $n( "scripts_add_async_defer_attrs" ), 10, 2 );
    add_action( "wp_head", $n( "supporting_javascript" ), 0 );
    add_action( "wp_head", $n( "pre_load_assets"), 1 );

  }

  /**
   * Echo list of scripts to be preloaded in site <head>.
   *
   * @return void
   */
  function pre_load_assets() {
    ProjectFonts\pre_load_fonts();
    ProjectScripts\pre_load_scripts();
  }

  /**
   * Register redirects.
   *
   * @return void
   */
  function redirects() {
    ProjectRedirects\init();
  }

  /**
   * Register and enqueue scripts.
   *
   * @return void
   */
  function scripts() {
    ProjectScripts\init();
  }

  /**
   * Add 'async' or 'defer' to scripts based on script $handle
   *
   * @param string $tag of script element
   * @param string $handle of script element
   * @return string $tag of (un)modified tag
   */
  function scripts_add_async_defer_attrs( $tag, $handle ) {

    // if the unique handle/name of the registered script has "async" in it
    if ( strpos($handle, "async") !== false ) {
      // return the tag with the async attribute
      return str_replace( "<script ", "<script async ", $tag );
    }

    // if the unique handle/name of the registered script has "defer" in it
    else if ( strpos($handle, "defer") !== false ) {
      // return the tag with the defer attribute
      return str_replace( "<script ", "<script defer ", $tag );
    }

    // otherwise skip
    else {
      return $tag;
    }

  }

  /**
   * Register and enqueue styles.
   *
   * @return void
   */
  function styles() {
    ProjectStyles\init();
  }

  /**
   * Echo inline JavaScript IIFE within site <head>.
   *
   * @return void
   */
  function supporting_javascript() {
    echo "
      <script>

        (function(html){

          html.className = 'js';

          html.style.setProperty( '--theme-viewport-height--total', window.innerHeight + 'px' );

          window.lazySizesConfig = window.lazySizesConfig || {};
          window.lazySizesConfig.lazyClass = 'lazyload';
          window.lazySizesConfig.loadMode = 1;

          document.addEventListener('lazybeforeunveil', function(e){
            let bg = e.target.getAttribute('data-bg');
            if ( bg ) e.target.style.backgroundImage = 'url(' + bg + ')';
          });

          document.addEventListener('lazyloaded', function(e) {
            e.target.parentNode.classList.add('lazyloaded');
          });

          WebFontConfig = { typekit: { id: 'bdj7xaw' } };

          var wf = document.createElement('script'), s = document.scripts[0];
          wf.src = 'https://cdn.jsdelivr.net/npm/webfontloader@1.6.28/webfontloader.min.js';
          wf.async = true;
          s.parentNode.insertBefore(wf, s);

        })(document.documentElement);

      </script>
    ";
  }
