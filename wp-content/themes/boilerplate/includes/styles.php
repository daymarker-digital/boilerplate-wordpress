<?php

  /**
   * Theme styles.
   *
   * @package WordPress
   * @subpackage boilerplate
   */

  namespace DD\Project\Styles;

  const STYLES = [
    "adobe-fonts" => [
      "src" => "https://use.typekit.net/bdj7xaw.css",
    ],
    "main" => [
      "src" => "/style.css",
    ]
  ];

  const UNUSED_STYLES = [
    "wp-block-library",
    "wp-block-library-theme",
    "wc-block-style",
    "storefront-gutenberg-blocks"
  ];

  /**
   * Initialize collection of style functions.
   *
   * @return void
   */
  function init() {
    remove_unused_styles();
    add_styles();
  }

  /**
   * Register and enqueue styles.
   *
   * @return void
   */
  function add_styles() {
    foreach( STYLES as $name => $style ) {

      global $wp_version;

      // Deconstruct data from $style
      $media = $style["media"] ?? "all";
      $deps = $style["deps"] ?? [];
      $src = $style["src"] ?? "";
      $ver = $style["ver"] ?? $wp_version;
      $handle = "{$name}-css";

      // Check if $src exists
      if ( !empty($src) ) {

        // If $src contains HTTP protocol
        if ( strpos( $src, "https://" ) === false ) {
          $path = DD_THEME_TEMPLATE_URL . $src;
          $version = filemtime( DD_THEME_PATH . $src );
        } else {
          $path = $src;
        }

        // Register and enqueue $path by $handle
        wp_register_style($handle, $path, $deps, $ver, $media);
        wp_enqueue_style($handle);

      }

    }
  }

  /**
   * Dequeue and deregister unused styles.
   *
   * @return void
   */
  function remove_unused_styles() {
    foreach( UNUSED_STYLES as $style ) {
      wp_dequeue_style($style);
      wp_deregister_style($style);
    }
  }
