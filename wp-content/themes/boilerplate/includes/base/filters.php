<?php

  /**
   * Base filters.
   *
   * @package WordPress
   * @subpackage boilerplate
   */

  namespace DD\Base\Filters;

  use function DD\Base\Tools\is_local;

  /**
   * Initialize collection of supported WordPress filters.
   *
   * @return void
   */
  function init() {

    $n = function( $function ) {
      return __NAMESPACE__ . "\\$function";
    };

    add_filter( "big_image_size_threshold", $n("image_size_threshold"), 999, 1 );
    add_filter( "body_class", $n("body_classes") );
    add_filter( "jpeg_quality", $n("image_quality") );
    add_filter( "upload_mimes", $n("add_to_mime_types") );

  }

  /**
   * Additional mime type support to the site.
   *
   * @param mixed $mimes array of mime types
   * @return mixed $mimes array of updated mime types
   */
  function add_to_mime_types( $mimes ){

    $mimes['json'] = 'text/plain';
    $mimes['svg'] = 'image/svg+xml';

    return $mimes;

  }

  /**
   * Addition of custom classes to the 'body_class' hook.
   *
   * @param mixed $classes array of body class names.
   * @return mixed $classes array of custom classes.
   */
  function body_classes( $classes ) {

    global $post;
    global $template;

    $post_ID = $post->ID ?? 0;
    $post_type = $post->post_type ?? "post-type-not-set";
    $post_slug = $post->post_name ?? "post-slug-not-set";
    $template = basename( $template, ".php" );

    $classes[] = "post-type--{$post_type}";
    $classes[] = "{$post_type}--{$post_slug}";
    $classes[] = "page-id--{$post_ID}";
    $classes[] = "template--{$template}";
    $classes[] = "{$template}";

    $classes[] = is_front_page() ? "is-front-page" : "";
    $classes[] = is_page() ? "is-page" : "";
    $classes[] = is_single() ? "is-single" : "";
    $classes[] = is_archive() ? "is-archive" : "";
    $classes[] = is_category() ? "is-category" : "";

    $classes[] = is_404() ? "is-404" : "";

    $classes[] = is_local() ? "env--development" : "env--production";

    return $classes;

  }

  /**
   * Set image quality.
   *
   * The filter is evaluated under two contexts: ‘image_resize’, and ‘edit_image’.
   *
   * @return int quality level between 0 (low) and 100 (high) of the JPEG.
   */
  function image_quality() {
    return 100;
  }

  /**
   * Set image threshold.
   *
   * If the original image width or height is above the threshold, it will be scaled down.
   * The threshold is used as max width and max height. The scaled down image will be used
   * as the largest available size, including the _wp_attached_file post meta value.
   * Returning false will disable the scaling.
   *
   * @return int image size threshold
   */
  function image_size_threshold( $threshold ) {
    return 6000;
  }


