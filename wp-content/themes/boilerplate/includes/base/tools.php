<?php

  /**
   * Base tools.
   *
   * This is a collection of miscellaneous helper functions.
   *
   * @package WordPress
   * @subpackage boilerplate
   */

  namespace DD\Base\Tools;

  /**
   * Echo's $object data and type to client for visual debugging.
   *
   * @param mixed $object variable to display with print_r()
   * @return void
   */
  function debug_this( $object = [] ) {

    $object_type = esc_html( gettype( $object ) );
    $object_info = esc_html( print_r( $object, true ) );

    echo "
      <div class='php-debugger' style='background: black; width: 90%; display: block; margin: 65px auto; padding: 40px 25px; position: relative; z-index: 10;'>
        <pre style='color: #fff;'>
          <h3 style='margin-bottom: 20px;'>Type: {$object_type}</h3>
          <h3 style='margin-bottom: 20px;'>Contents of {$object_type}:</h3>
          {$object_info}
        </pre>
      </div>
    ";

  }

  /**
   * Returns true if post slug is not in $blacklist array or is not 404 page.
   *
   * @return bool
   */
  function enable_footer() {

    global $post;

    // Assign post name slug to $post_slug (with fallback)
    $post_slug = $post->post_name ?? "no-post-slug";

    // Configure blacklist of post slugs
    $blacklist = [ "thank-you" ];

    // Return false if currently 404 or if $post_slug is in $blacklist
    if ( is_404() || in_array( $post_slug, $blacklist ) ) {
      return false;
    }

    return true;

  }

  /**
   * Returns true if post slug is not in $blacklist array or is not 404 page.
   *
   * @return bool
   */
  function enable_header() {

    global $post;

    // Assign post name slug to $post_slug (with fallback)
    $post_slug = $post->post_name ?? "no-post-slug";

    // Configure blacklist of post slugs
    $blacklist = [ "thank-you" ];

    // Return false if currently 404 or if $post_slug is in $blacklist
    if ( is_404() || in_array( $post_slug, $blacklist ) ) {
      return false;
    }

    return true;

  }

  /**
   * Returns block id.
   *
   * @param mixed $block to use in order to extract id
   * @return string
   */
  function get_block_id_from_block($block=[]) {
    return ( isset($block["anchor"]) && ! empty($block["anchor"]) ) ? $block["anchor"] : $block["id"];
  }

  /**
   * Returns featured image.
   *
   * @param int $post_id to fetch post's featured image
   * @return mixed
   */
  function get_featured_image_by_post_id( $post_id = 0 ) {

    // Assign default value for $image
    $image = [];

    // Check for $post_id
    if ( $post_id ) {

      // Get registered image sizes
      $sizes = get_intermediate_image_sizes();

      // Get featured image from $post_id
      $post_thumbnail_id = get_post_thumbnail_id( $post_id );

      // If $sizes and $post_thumbnail_id, construct associative array for featured image
      if ( $sizes && $post_thumbnail_id ) {

        $image_alt = get_post_meta( $post_thumbnail_id , '_wp_attachment_image_alt', true );
        $image_attributes = wp_get_attachment_image_src( $post_thumbnail_id, "full" );
        $image_filetype = wp_check_filetype( get_the_post_thumbnail_url( $post_id ) );

        $image["url"] = get_the_post_thumbnail_url( $post_id );
        $image["sizes"] = [];
        $image["alt"] = $image_alt;
        $image["width"] = $image_attributes[1];
        $image["height"] = $image_attributes[2];
        $image["ext"] = $image_filetype["ext"] ?: "";

        foreach ( $sizes as $index => $size ) {
          $image["sizes"][$size] = wp_get_attachment_image_src( $post_thumbnail_id, $size )[0];
        }

      }

    }

    return $image;

  }

  /**
   * Returns handeleized version of string.
   *
   * @param string $string to convert into handle
   * @return string
   */
  function handleize( $string = "" ) {

    // Removes html
    $string = strip_tags($string);

    // Lower case everything
    $string = strtolower($string);

    // Make alphanumeric (removes all other characters)
    $string = preg_replace( "/[^a-z0-9_\s-]/", "", $string );

    // Clean up multiple dashes or whitespaces
    $string = preg_replace( "/[\s-]+/", " ", $string );

    // Convert whitespaces and underscore to dash
    $string = preg_replace( "/[\s_]/", "-", $string );

    return $string;

  }

  /**
   * Checks if the current environment is local or production
   *
   * @return bool
   */
  function is_local() {

    // Check to see if local environment type is either "local" or "development"
    $is_local_env = in_array( wp_get_environment_type(), [ "local", "development" ], true );

    // Check if home_url contains either ".test", ".local" or "wp-"
    $is_local_url = strpos( home_url(), ".test" ) || strpos( home_url(), ".local" ) || strpos( home_url(), "wp-" );

    // If either is true, return true
    return $is_local_env || $is_local_url;

  }

  /**
   * Trims string to specific length with a custom ending.
   *
   * @param string $text to trim
   * @param int $maxchar to use as a limit for the max characters trimmed to
   * @param string $end to append the end of the trimmed string
   * @return string
   */
  function trim_string( $text = "", $maxchar = 200, $end = "..." ) {

      $i = 0;
      $output = '';
      $length = 0;

      // If $text has enough characters to be trimmed
      if ( strlen($text) > $maxchar ) {

        // Spite $text into array of substrings of words
        $words = preg_split('/\s/', $text);

        // Initialize infinite while loop
        while (true) {

          // Recursively calculate $length of characters
          $length = strlen($output) + strlen($words[$i]);

          // Exit while loop if $length is greater than $maxchar
          if ($length > $maxchar) {
            break;

          // Else continue concatenating $output
          } else {
            $output .= " " . $words[$i];
            ++$i;
          }

        }

        $output .= $end;

      } else {

        $output = $text;

      }

    return $output;

  }

  /**
   * Returns a unique ID that is safe the HTLM id attribute.
   *
   * @param string $prefix to prefix the unique ID with
   * @return string
   */
  function unique_id( $prefix = "id--" ) {
    return trim( $prefix . md5( uniqid( rand(), true ) ) );
  }

  /**
   * Warning message to client via console.log for debugging purposes.
   *
   * @param string $message to be printed to console
   * @return void
   */
  function warning_message( $message = "Missing arguments or data!" ) {
    echo "<script>console.log('" . esc_js($message) . "');</script>";
  }
