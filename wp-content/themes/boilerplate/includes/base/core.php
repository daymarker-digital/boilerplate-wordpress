<?php

  /**
   * Base core actions.
   *
   * @package WordPress
   * @subpackage boilerplate
   */

  namespace DD\Base\Core;

  const IMAGE_SIZES = [ 1, 10, 90, 180, 360, 540, 720, 900, 1080, 1296, 1512, 1728, 2048 ];
  const IMAGE_SIZES_LABEL = "dd-image-size";

  /**
   * Initialize collection of supported core WordPress actions.
   *
   * @return void
   */
  function init() {

    $n = function( $function ) {
	    return __NAMESPACE__ . "\\$function";
    };

    add_action( "after_setup_theme", $n( "theme_setup" ) );
    add_action( "after_setup_theme", $n( "register_image_sizes" ) );
    add_action( "wp_head", $n( "favicons" ), 1 );
    add_action( "wp_head", $n( "seo" ), 1 );
    add_action( "send_headers", $n( "security_block_frames"), 10 );
    add_action( "admin_head", $n( "svg_fix" ) );

  }

  /**
   * Echo favicons within site <head>.
   *
   * @return void
   */
  function favicons() {

    $apple_touch = DD_THEME_TEMPLATE_URL . "/apple-touch-icon.png?v=" . filemtime( DD_THEME_PATH . "/apple-touch-icon.png" );
    $favicon = DD_THEME_TEMPLATE_URL . "/favicon.ico?v=" . filemtime( DD_THEME_PATH . "/favicon.ico" );

    echo "
      <link rel='apple-touch-icon' type='image/x-icon' href='" . esc_url($apple_touch) . "'>
      <link rel='shortcut icon' type='image/x-icon' href='" . esc_url($favicon) . "'>
    ";

  }

  /**
   * Register theme support.
   *
   * @return void
   */
  function theme_setup() {
    add_theme_support( "automatic-feed-links" );
    add_theme_support( "title-tag" );
    add_theme_support( "post-thumbnails" );
    add_theme_support( "editor-styles" );
    add_theme_support( 'wp-block-styles' );
    add_theme_support( 'custom-spacing' );
  }
  /**
   * Register custom image sizes.
   *
   * @return void
   */
  function register_image_sizes() {
    foreach ( IMAGE_SIZES as $size ) {
	    $size_title = IMAGE_SIZES_LABEL . "-" . $size;
	    add_image_size( $size_title, $size, 9999 );
    }
  }

  /**
   * Echo basic SEO within site <head>.
   *
   * @return void
   */
  function seo() {

    $meta_desc = get_bloginfo( "description" ) ?: "";
    $is_attachment = is_attachment( get_queried_object_id() ?: 0 );
    $yoast_active = defined( "WPSEO_VERSION" ) ?: false;

    // If current page is an attachment, hide from robots
    if ( $is_attachment ) {
      echo "<meta name='robots' content='noindex, nofollow'>";

    // Echo meta data if Yoast isn't active
    } else {
      if ( ! $yoast_active ) {
        echo "<meta name='description' content='" . esc_attr($meta_desc) . "'>";
      }
    }

  }

  /**
   * Security hook that block external frames
   *
   * @return void
   */
  function security_block_frames() {
    header( 'X-FRAME-OPTIONS: SAMEORIGIN' );
  }

  /**
   * SVG fix with inline CSS
   *
   * @return void
   */
  function svg_fix() {
    echo "
      <style type='text/css'>
        .attachment-266x266, .thumbnail img {
          width: 100% !important;
          height: auto !important;
        }
      </style>
    ";
  }
