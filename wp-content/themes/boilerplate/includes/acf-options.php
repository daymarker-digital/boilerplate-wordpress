<?php

  /**
   * Theme ACF Options.
   *
   * @package WordPress
   * @subpackage boilerplate
   */

  namespace DD\Project\ACFOptions;
  use function DD\Base\Tools\handleize;

  const OPTIONS_SLUG = "theme-settings";
  const OPTIONS_PAGES = [
    "Company Info",
    "Social",
    "Mobile Menu",
    "Header",
    "Footer",
    "404"
  ];

  /**
   * Initialize functions.
   *
   * @return void
   */
  function init() {

    $n = function( $function ) {
      return __NAMESPACE__ . "\\$function";
    };

    add_action( "acf/init", $n( "theme_settings" ) );

  }

  /**
   * Configure ACF theme options/settings pages.
   *
   * @return void
   */
  function theme_settings() {
    if ( function_exists("acf_add_options_page") ) {

      $options_slug = OPTIONS_SLUG;

      acf_add_options_page([
        "page_title" => "Theme Settings",
        "menu_title" => "Theme Settings",
        "menu_slug" => $options_slug,
        "capability" => "edit_posts",
        "parent_slug" => "",
        "position" => "2.1",
        "icon_url" => false
      ]);

      foreach ( OPTIONS_PAGES as $title ) {
        $title_slug = handleize($title);
        acf_add_options_sub_page([
          "capability" => "edit_posts",
          "menu_slug" => "{$options_slug}-{$title_slug}",
          "menu_title" => $title,
          "page_title" => $title,
          "parent_slug" => $options_slug,
        ]);
      }

    }
  }
