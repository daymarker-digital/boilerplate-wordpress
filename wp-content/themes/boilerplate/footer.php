<?php

  /**
   * The template for displaying the footer.
   *
   * @package WordPress
   * @subpackage boilerplate
   */

  use function DD\Base\Tools\enable_footer;

?>

      </div>
    </div>
  </main>

  <?php if ( enable_footer() ) : ?>
    <?php get_template_part( "partials/footer" ); ?>
  <?php endif; ?>

  <?php wp_footer(); ?>

  </body>
</html>
