<?php

  /**
   * Filename: 404.php
   *
   * The main template file for missing content or pages.
   *
   * @package WordPress
   * @subpackage boilerplate
   */

  get_header();

  get_footer();
