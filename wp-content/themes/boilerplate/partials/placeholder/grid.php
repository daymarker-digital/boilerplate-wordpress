<?php

  /**
   * The template for placeholder grid layout.
   *
   * @package WordPress
   * @subpackage boilerplate
   */

?>

<section class="placeholder">
  <div class="placeholder__grid">

    <h1>H1 - Placeholder Grid</h1>

    <?php foreach ( [ 12, 6, 4, 3, 2 ] as $cols ) : ?>
      <h2><?php echo $cols; ?> Column Grid</h2>
      <div class="row row--inner">
        <?php for ( $i = 1; $i <= $cols; $i++ ) : $col = 12/$cols; ?>
          <div class="col-<?php echo $col; ?>">
            <span><?php echo $i; ?></span>
          </div>
        <?php endfor; ?>
      </div>
    <?php endforeach; ?>

  </div>
</section>
