<?php

  /**
   * The template for single marquee.
   *
   * @package WordPress
   * @subpackage boilerplate
   */

  /*
   * Receives mixed[] $args Array structure from invoking template.
   * Default arguments included.
   */
  $args = wp_parse_args($args, [
    "items" => [],
    "direction" => "ltr",
    "size" => "large",
  ]);

?>

<?php if ( !empty($args["items"]) ) : ?>

  <div class="marquee" data-direction="<?php echo esc_attr($args["direction"]); ?>">

    <div class="marquee__content">
      <?php for ( $i = 0; $i < 2; $i++ ) : ?>
        <?php foreach ( $args["items"] as $item ) : ?>
          <div class="marquee__item marquee__item--<?php echo esc_attr($args["size"]); ?>" <?php if ( $i > 0 ) : ?>aria-hidden="true"<?php endif; ?>><?php echo $item; ?></div>
        <?php endforeach; ?>
      <?php endfor; ?>
    </div>

    <div class="marquee__content" aria-hidden="true">
      <?php for ( $i = 0; $i < 2; $i++ ) : ?>
        <?php foreach ( $args["items"] as $item ) : ?>
          <div class="marquee__item marquee__item--<?php echo esc_attr($args["size"]); ?>"><?php echo $item; ?></div>
        <?php endforeach; ?>
      <?php endfor; ?>
    </div>

  </div>

<?php endif; ?>
