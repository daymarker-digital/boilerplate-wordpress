<?php

  /**
   * Template the media.
   *
   * @package WordPress
   * @subpackage boilerplate
   */

  use function DD\Base\Tools\get_featured_image_by_post_id;

  /*
   * Receives mixed[] $args Array structure from invoking template.
   * Default arguments included.
   */
  $args = wp_parse_args($args, [
    "image" => [],
    "type" => "featured",
    "lottie" => [],
    "post_id" => 0,
    "video" => []
  ]);

  switch ( $args["type"] ) {
    case "featured": {
      get_template_part( "partials/content/image", null, get_featured_image_by_post_id($args["post_id"]) );
      break;
    }
    case "image": {
      get_template_part( "partials/content/image", null, $args["image"] );
      break;
    }
    case "lottie": {
      get_template_part( "partials/content/lottie", null, $args["lottie"] );
      break;
    }
    case "video": {
      get_template_part( "partials/content/video", null, $args["video"] );
      break;
    }
    default: {
      echo "<p>Oops! Media type not found.</p>";
      break;
    }
  }

