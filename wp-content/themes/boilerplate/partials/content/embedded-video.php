<?php

  /**
   * Template for embedded video (Vimeo, YouTube, etc.)
   *
   * @package WordPress
   * @subpackage boilerplate
   */

  /*
   * Receives mixed[] $args Array structure from invoking template.
   * Default arguments included.
   */
  $args = wp_parse_args($args, [
    "embed_controls" => false,
    "embed_height" => "900",
    "embed_id" => 160730254,
    "embed_src" => "vimeo",
    "embed_width" => "1600",
  ]);

  $embed_classes = "embed";
  $embed_controls = $args["embed_controls"] === "yes";
  $embed_height = $args["embed_height"];
  $embed_id = $args["embed_id"];
  $embed_src = $args["embed_src"];
  $embed_width = $args["embed_width"];

  switch( $embed_src ) {
    case "vimeo": {
      $embed_src = "https://player.vimeo.com/video/{$embed_id}";
      $embed_src .= $embed_controls ? "?controls=1&keyboard=1&background=0&loop=0" : "?controls=0&keyboard=0&background=1&loop=1";
      break;
    }
    case "youtube": {
      $embed_src = "https://www.youtube.com/embed/{$embed_id}";
      $embed_src .= $embed_controls ? "?controls=1&disablekb=0&loop=0" : "?controls=0&disablekb=1&loop=1";
      break;
    }
    default: {
      $embed_src = "";
      break;
    }
  }

?>

<?php if ( $embed_id ) : ?>
  <iframe
    class="<?php echo esc_attr($embed_classes); ?>"
    loading="lazy"
    src="<?php echo esc_url($embed_src); ?>"
    <?php if ( !empty($embed_width) ) : ?>
      width="<?php echo esc_attr($embed_width); ?>"
    <?php endif; ?>
    <?php if ( !empty($embed_height) ) : ?>
      height="<?php echo esc_attr($embed_height); ?>"
    <?php endif; ?>
    frameborder="0"
    allow="autoplay; encrypted-media"
    webkitallowfullscreen mozallowfullscreen allowfullscreen
  ></iframe>
<?php else: ?>
  <p>Oops! Missing embed ID.</p>
<?php endif; ?>
