<?php

  /**
   * Template for message.
   *
   * @package WordPress
   * @subpackage tenaciousdigital
   */

  /*
   * Receives mixed[] $args Array structure from invoking template.
   * Default arguments included.
   */
  $args = wp_parse_args($args, [
    "content" => "",
    "classes" => [ "message" ],
  ]);

  $classes = implode(" ", $args["classes"]);

?>

<?php if ( ! empty($args["content"]) ) : ?>
  <div class="<?php echo esc_attr($classes); ?>"><?php echo wp_kses_post($args["content"]); ?></div>
<?php endif; ?>
