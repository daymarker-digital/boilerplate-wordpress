<?php

  /**
   * Template for video (MP4, etc.).
   *
   * @package WordPress
   * @subpackage boilerplate
   */

  /*
   * Receives mixed[] $args Array structure from invoking template.
   * Default arguments included.
   */
  $args = wp_parse_args($args, [
    "classes" => [],
    "js_lazyload" => true,
    "loop" => true,
    "playback" => "autoplay",
    "source" => "external",
    "source_external" => "",
    "source_internal" => [],
  ]);

  // Build video source
  switch ( $args["source"] ) {
    case "external": {
      $src = $args["source_external"] ?? "";
      break;
    }
    case "internal": {
      $src = $args["source_internal"]["url"] ?? "";
      break;
    }
    default: {
      $src = "";
      break;
    }
  }

  // Extract mime_type
  if ( str_contains($src, ".mp4") ) {
    $src_mime_type = "video/mp4";
  } elseif ( str_contains($src, ".mpeg") ) {
    $src_mime_type = "video/mpeg";
  } elseif ( str_contains($src, ".webm") ) {
    $src_mime_type = "video/webm";
  } else {
    $src_mime_type = "";
  }

  // Build classes
  $classes = "video";
  if ( ! empty($args["classes"]) ) {
    foreach( $args["classes"] as $item ) {
      $classes .= " {$item}";
    }
  }
  if ( $args["js_lazyload"] ) {
    $classes .= " lazyload lazyload-item";
  }

?>

<?php if ( ! empty($src) ) : ?>
  <video
    class="<?php echo esc_attr($classes); ?>"
    src="<?php echo esc_url($src); ?>"
    <?php if ( $args["js_lazyload"] ) : ?>
      data-transition-delay="0"
      data-transition-duration="750"
      <?php if ( "autoplay" === $args["playback"] ) : ?> data-autoplay=""<?php endif; ?>
      data-poster=""
    <?php else : ?>
      loading="lazy"
    <?php endif; ?>
    <?php if ( $args["loop"] ) : ?>loop<?php endif; ?>
    <?php if ( "autoplay" === $args["playback"] ) : ?>autoplay<?php endif; ?>
    muted
    defaultMuted
    preload
    playsinline
  >
    <source src="<?php echo esc_url($src); ?>" type="<?php echo esc_attr($src_mime_type); ?>">
  </video>
<?php else: ?>
  <p>Oops! Missing video source.</p>
<?php endif; ?>
