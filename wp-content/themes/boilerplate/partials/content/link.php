<?php

  /**
   * Template for link.
   *
   * @package WordPress
   * @subpackage tenaciousdigital
   */

  /*
   * Receives mixed[] $args Array structure from invoking template.
   * Default arguments included.
   */
  $args = wp_parse_args($args, [
    "anchor" => "",
    "appearance" => "link",
    "classes" => [ "link" ],
    "current_id" => 0,
    "active" => false,
    "link_email" => "",
    "link_external" => "",
    "link_file" => "",
    "link_page" => 0,
    "link_phone" => "",
    "style" => "primary",
    "target" => "_blank",
    "title" => "",
    "type" => "",
  ]);

  switch ( $args["type"] ) {
    case "anchor" : {
      $link = get_permalink($args["link_page"]);
      $link .= $args["anchor"];
      $args["target"] = "_self";
      break;
    }
    case "email" : {
      $link = "mailto:" . $args["link_email"];
      break;
    }
    case "external" : {
      $link = $args["link_external"];
      break;
    }
    case "page" : {
      $link = get_permalink($args["link_page"]);
      $args["active"] = $args["current_id"] === $args["link_page"] ? true : false;
      $args["target"] = "_self";
      break;
    }
    default: {
      $link = "";
      break;
    }
  }

  $args["classes"][] = $args["active"] ? "active" : "not-active";
  $classes = implode(" ", $args["classes"]);
  $target = $args["target"];
  $title = $args["title"];

?>

<?php if ( ! empty($title) && ! empty($link) ) : ?>
  <a
    class="<?php echo esc_attr($classes); ?>"
    href="<?php echo esc_url($link); ?>"
    target="<?php echo esc_attr($target); ?>"
    title="<?php echo esc_attr($title); ?>"
  >
    <span class="link__title"><?php echo esc_html($title); ?></span>
  </a>
<?php endif; ?>
