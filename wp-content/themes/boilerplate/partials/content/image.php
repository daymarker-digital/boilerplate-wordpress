<?php

  /**
   * Template for image.
   *
   * @package WordPress
   * @subpackage boilerplate
   */

  use function DD\Base\Tools\{get_featured_image_by_post_id, unique_id};

  use const DD\Base\Core\IMAGE_SIZES_LABEL as CORE_IMAGE_SIZES_LABEL;
  use const DD\Base\Core\IMAGE_SIZES as CORE_IMAGE_SIZES;

  /*
   * Receives mixed[] $args Array structure from invoking template.
   * Default arguments included.
   */
  $args = wp_parse_args($args, [
    "alt" => "",
    "classes" => [],
    "id" => "",
    "js_lazyload" => true,
    "url" => "",
    "sizes" => [],
    "width" => 0,
    "height" => 0,
    "ext" => ""
  ]);

  $alt = $args["alt"] ?: get_bloginfo("name") . " Photograph";
  $sizes = $args["sizes"] ?? [];
  $src = $args["url"] ?? "";
  $src_thumbnail = $sizes["thumbnail"] ?? "";
  $filetype = $args["ext"] ?: wp_check_filetype($src)["ext"];

  $srcset_enabled = "svg" === $filetype || "gif" === $filetype ? false : true;
  $srcset_arr = [];
  $srcset_str = "";

  // Build srcset if not GIF or SVG
  if ( $srcset_enabled ) {
    if ( !empty($sizes) ) {
      foreach ( CORE_IMAGE_SIZES as $size ) {
        if ( $size > 300 && $size < 2000 ) {
          $srcset_key = CORE_IMAGE_SIZES_LABEL . "-" . $size;
          $srcset_item = $sizes[$srcset_key] . " " . $size . "w";
          array_push($srcset_arr, $srcset_item);
        }
      }
      array_push($srcset_arr, $src . " 2000w");
    }
    $srcset_str = implode(", ", $srcset_arr);
  }

  // Build classes
  $classes = "image";
  if ( ! empty($args["classes"]) ) {
    foreach( $args["classes"] as $item ) {
      $classes .= " {$item}";
    }
  }
  if ( $args["js_lazyload"] ) {
    $classes .= " lazyload lazyload-item";
  }

  $id = empty($args["id"]) ? unique_id("image--") : $args["id"];

?>

<?php if ( ! empty($src) ) : ?>
  <img
    alt="<?php echo esc_attr($alt); ?>"
    class="<?php echo esc_attr($classes); ?>"
    id="<?php echo esc_attr($id); ?>"
    <?php if ( $args["js_lazyload"] ) : ?>
      src="<?php echo esc_url($src_thumbnail); ?>"
      data-src="<?php echo esc_url($src); ?>"
      data-transition-delay="0"
      data-transition-duration="350"
      <?php if ( $srcset_enabled && ! empty($srcset_str) ) : ?>
        data-sizes="100vw" data-srcset="<?php echo esc_attr($srcset_str); ?>"
      <?php endif; ?>
    <?php else : ?>
      src="<?php echo esc_url($src); ?>"
      loading="lazy"
    <?php endif; ?>
    <?php if ( $srcset_enabled && ! empty($srcset_str) ) : ?>
      sizes="100vw" srcset="<?php echo esc_attr($srcset_str); ?>"
    <?php endif; ?>
    <?php if ( ! empty($args["width"]) ) : ?>
      width="<?php echo esc_attr($args["width"]); ?>"
    <?php endif; ?>
    <?php if ( ! empty($args["height"]) ) : ?>
      height="<?php echo esc_attr($args["height"]); ?>"
    <?php endif; ?>
  />
<?php endif; ?>

