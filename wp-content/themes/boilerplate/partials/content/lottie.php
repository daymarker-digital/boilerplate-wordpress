<?php

  /**
   * Template for lottie.
   *
   * @package WordPress
   * @subpackage boilerplate
   */

  use function DD\Base\Tools\unique_id;

  /*
   * Receives mixed[] $args Array structure from invoking template.
   * Default arguments included.
   */
  $args = wp_parse_args($args, [
    "direction" => "forward",
    "loop" => true,
    "play_mode" => "normal",
    "playback" => "autoplay",
    "speed" => 1,
    "src" => "",
    "source" => "external",
    "source_external" => "",
    "source_internal" => []
  ]);

  switch ( $args["source"] ) {
    case "external": {
      $src = $args["source_external"] ?? "";
      break;
    }
    case "internal": {
      $src = $args["source_internal"]["url"] ?? "";
      break;
    }
    default: {
      $src = "";
      break;
    }
  }

  $classes = "lottie js--lottie";
  $classes .= "in-view" === $args["playback"] ? " js--play-in-view" : "";
  $direction = "forward" === $args["direction"] ? 1 : -1;
  $id = unique_id("lottie--");

?>

<?php if ( $src ) : ?>
  <lottie-player
    class="<?php echo esc_attr($classes); ?>"
    id="<?php echo esc_attr($id); ?>"
    src="<?php echo esc_url($src); ?>"
    background="transparent"
    speed="<?php echo esc_attr($args["speed"]); ?>"
    direction="<?php echo esc_attr($direction); ?>"
    mode="<?php echo esc_attr($args["play_mode"]); ?>"
    <?php if ( $args["loop"] ) : ?>
      loop
    <?php endif; ?>
    <?php if ( "autoplay" === $args["playback"] ) : ?>
      autoplay
    <?php endif; ?>
  ></lottie-player>
<?php endif; ?>
