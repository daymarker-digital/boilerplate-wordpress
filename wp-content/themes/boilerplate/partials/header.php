<?php

  /**
   * The template for the header.
   *
   * @package WordPress
   * @subpackage boilerplate
   */

  // Partial data.
  $partial_name = "header";
  $partial_classes = $partial_name;
  $partial_id = $partial_name;

  // Content (ACF) data.
  $header_options = get_field( "header", "options" ) ?: [];
  $navigation = $header_options["navigation"] ?? [];

?>

<header class="<?php echo esc_attr($partial_classes); ?>" id="<?php echo esc_attr($partial_id); ?>">
  <div class="<?php echo esc_attr($partial_classes); ?>__flex-container flex-container">

    <div class="<?php echo esc_attr($partial_classes); ?>__brand">
      <a class="<?php echo esc_attr($partial_classes); ?>__brand-link link" href="/" target="_self" title="Home">
        <?php get_template_part( "partials/svg/brand.logo" ); ?>
      </a>
    </div>

    <button class="<?php echo esc_attr($partial_classes); ?>__button js--toggle-drawer" data-drawer-id="drawer-mobile-menu"></button>

    <?php if ( !empty($navigation) ) : ?>
      <nav class="navigation" role="navigation" aria-label="Header Navigation">
        <?php foreach ( $navigation as $item ) : $item["link_style"] = "secondary"; ?>
          <div class="navigation__item">
            <?php get_template_part( "partials/link", null, $item ); ?>
          </div>
        <?php endforeach; ?>
      </nav>
    <?php endif; ?>

  </div>
</header>
