<?php

  /**
   * The template for an element's inline styles.
   *
   * @package WordPress
   * @subpackage boilerplate
   */

  /*
   * Receives mixed[] $args Array structure from invoking template.
   * Default arguments included.
   */
  $args = wp_parse_args($args, [
    "background_colour" => "white",
    "id" => "",
    "padding_bottom" => 0,
    "padding_top" => 0,
    "text_colour" => "black",
  ]);

?>

<?php if ( !empty($args["id"]) ) : ?>
  #<?php echo esc_html($args["id"]); ?> {
    <?php if ( "none" !== $args["background_colour"] ) : ?>
      background: var(--theme-colour--<?php echo esc_html($args["background_colour"]); ?>);
    <?php endif; ?>
    <?php if ( "none" !== $args["text_colour"] ) : ?>
      color: var(--theme-colour--<?php echo esc_html($args["text_colour"]); ?>);
    <?php endif; ?>
    <?php if ( "initial" !== $args["padding_top"] ) : ?>
      padding-top: calc(<?php echo esc_html($args["padding_top"]); ?>px * 0.6);
    <?php endif; ?>
    <?php if ( "initial" !== $args["padding_bottom"] ) : ?>
      padding-bottom: calc(<?php echo esc_html($args["padding_bottom"]); ?>px  * 0.6);
    <?php endif; ?>
  }
  @media screen and (min-width: 992px) {
    #<?php echo esc_html($args["id"]); ?> {
      <?php if ( "initial" !== $args["padding_top"] ) : ?>
        padding-top: <?php echo esc_html($args["padding_top"]); ?>px;
      <?php endif; ?>
      <?php if ( "initial" !== $args["padding_bottom"] ) : ?>
        padding-bottom: <?php echo esc_html($args["padding_bottom"]); ?>px;
      <?php endif; ?>
    }
  }
<?php endif; ?>
