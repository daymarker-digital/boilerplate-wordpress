<?php

  /**
   * The template for lazyloaded embed.
   *
   * @package WordPress
   * @subpackage boilerplate
   */

  use function DD\Base\Tools\warning_message;

  /*
   * Receives mixed[] $args Array structure from invoking template.
   * Default arguments included.
   */
  $args = wp_parse_args($args, [
    "classes" => "embed lazyload lazypreload lazyload-item lazyload-item--embed",
    "controls" => false,
    "delay" => 0,
    "duration" => 750,
    "embed_id" => 160730254,
    "embed_src" => "vimeo",
  ]);

  // Conditionally constructs $embed_src based on "embed_src".
  switch ($args["embed_src"]) {
    case "vimeo": {
      $embed_src = "https://player.vimeo.com/video/" . $args["embed_id"];
      $embed_src .= $args["controls"] ? "?controls=1&keyboard=1&background=0&loop=0" : "?controls=0&keyboard=0&background=1&loop=1";
      break;
    }
    case "youtube": {
      $embed_src = "https://www.youtube.com/embed/" . $args["embed_id"];
      $embed_src .= $args["controls"] ? "?controls=1&disablekb=0&loop=0" : "?controls=0&disablekb=1&loop=1";
      break;
    }
    default: {
      $embed_src = "";
      break;
    }
  }

?>

<?php if ($args["embed_id"] && $embed_src) : ?>
  <iframe
    class="<?php echo esc_attr($args["classes"]); ?>"
    data-src="<?php echo esc_url($args["embed_src"]); ?>"
    data-transition-delay="<?php echo esc_attr($args["delay"]); ?>"
    data-transition-duration="<?php echo esc_attr($args["duration"]); ?>"
    frameborder="0"
    allow="autoplay; encrypted-media"
    webkitallowfullscreen mozallowfullscreen allowfullscreen
  ></iframe>
<?php else: ?>
  <?php warning_message("Missing embed_id and embed_src!"); ?>
<?php endif; ?>
