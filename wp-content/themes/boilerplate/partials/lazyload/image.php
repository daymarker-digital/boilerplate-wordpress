<?php

  /**
   * The template for lazyloaded image.
   *
   * @package WordPress
   * @subpackage boilerplate
   */

  use const DD\Base\Core\IMAGE_SIZES_LABEL as CORE_IMAGE_SIZES_LABEL;
  use const DD\Base\Core\IMAGE_SIZES as CORE_IMAGE_SIZES;
  use function DD\Base\Tools\warning_message;

  /*
   * Receives mixed[] $args Array structure from invoking template.
   * Default arguments included.
   */
  $args = wp_parse_args($args, [
    "alt" => "",
    "classes" => "image lazyload lazypreload lazyload-item lazyload-item--image",
    "delay" => 0,
    "duration" => 250,
    "image" => [],
    "with_js" => true,
  ]);

  // Construct classes
  $classes = empty($args["classes"]) ? $args["classes"] . " image" : "image";
  $classes .= $args["with_js"] ? " with-js lazyload lazypreload lazyload-item lazyload-item--image" : " without-js";

  // Construct relevant image attributes.
  $image = $args["image"];
  $image_alt = $args["alt"] ?: ( (isset($image["alt"]) && !empty($image["alt"])) ? $image["alt"] : get_bloginfo("name") . " Photograph" );
  $image_sizes = $image["sizes"] ?? [];
  $image_src = $image["url"] ?? "";
  $image_src_small = $image_sizes[CORE_IMAGE_SIZES_LABEL . "-10"];
  $image_type = $image["subtype"] ?? "";
  $image_height = $image["height"] ?? "";
  $image_width = $image["width"] ?? "";
  $include_srcset = "svg+xml" === $image_type || "gif" === $image_type ? false : true;
  $srcset_arr = [];
  $srcset_str = "";

  // If image type is not SVG or GIF, construct image srcset values
  if ( $include_srcset ) {
    if ( !empty($image_sizes) ) {
      foreach ( CORE_IMAGE_SIZES as $size ) {
        if ( $size > 300 && $size < 2000 ) {
          $srcset_key = CORE_IMAGE_SIZES_LABEL . "-" . $size;
          $srcset_item = $image_sizes[$srcset_key] . " " . $size . "w";
          array_push($srcset_arr, $srcset_item);
        }
      }
      array_push($srcset_arr, $image_src . " 2000w");
    }
    $srcset_str = implode(", ", $srcset_arr);
  }

?>

<?php if ( !empty($image_src) ) : ?>
  <img
    alt="<?php echo esc_attr($image_alt); ?>"
    class="<?php echo esc_attr($classes); ?>"
    data-image-type="<?php echo esc_attr($image_type); ?>"
    <?php if ( $args["with_js"] ) : ?>
      src="<?php echo esc_url($image_src_small); ?>"
      data-src="<?php echo esc_url($image_src); ?>"
      <?php if ( $include_srcset && !empty($srcset_str) ) : ?>
        data-sizes="100vw" data-srcset="<?php echo esc_attr($srcset_str); ?>"
      <?php endif; ?>
    <?php else : ?>
      loading="lazy"
      src="<?php echo esc_url($image_src); ?>"
      <?php if ( $include_srcset && !empty($srcset_str) ) : ?>
        sizes="100vw" srcset="<?php echo esc_attr($srcset_str); ?>"
      <?php endif; ?>
    <?php endif; ?>
    <?php if ( "none" !== $args["transition"] ) : ?>
      data-transition="<?php echo esc_attr($args["transition"]); ?>"
      data-transition-delay="<?php echo esc_attr($args["delay"]); ?>"
      data-transition-duration="<?php echo esc_attr($args["duration"]); ?>"
    <?php endif; ?>
    <?php if ( !empty($image_width) ) : ?>
      width="<?php echo esc_attr($image_width); ?>"
    <?php endif; ?>
    <?php if ( !empty($image_height) ) : ?>
      height="<?php echo esc_attr($image_height); ?>"
    <?php endif; ?>
  />
<?php else : ?>
  <?php warning_message("Missing image_src!"); ?>
<?php endif; ?>
