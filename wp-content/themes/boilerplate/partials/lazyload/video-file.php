<?php

  /**
   * The template for lazyloaded video file.
   *
   * @package WordPress
   * @subpackage boilerplate
   */

  use function DD\Base\Tools\warning_message;

  /*
   * Receives mixed[] $args Array structure from invoking template.
   * Default arguments included.
   */
  $args = wp_parse_args($args, [
    "autoplay" => false,
    "classes" => "video-file lazyload lazypreload lazyload-item lazyload-item--video-file",
    "controls" => false,
    "delay" => 0,
    "duration" => 750,
    "html" => "",
    "loop" => false,
    "muted" => false,
    "video_file" => [],
  ]);

  // Deconstruct video attributes from provided arguments.
  $video_file = $args["video_file"];
  $video_src = $video_file["url"] ?? "";
  $video_mime_type = $video_file["mime_type"] ?? "";

?>

<?php if ( !empty($video_src) ) : ?>
  <video
    class="<?php echo esc_attr($args["classes"]); ?>"
    data-transition-delay="<?php echo esc_attr($args["delay"]); ?>"
    data-transition-duration="<?php echo esc_attr($args["duration"]); ?>"
    <?php if ( $args["autoplay"] ) : ?>autoplay<?php endif; ?>
    <?php if ( $args["controls"] ) : ?>controls<?php endif; ?>
    <?php if ( $args["loop"] ) : ?>loop<?php endif; ?>
    <?php if ( $args["muted"] ) : ?>muted<?php endif; ?>
    preload=""
  >
    <source src="<?php echo esc_url($video_src); ?>" type="<?php echo esc_attr($video_mime_type); ?>">
  </video>
<?php else: ?>
  <?php warning_message("Missing video_src!"); ?>
<?php endif; ?>
