<?php

  /**
   * The template for single social item.
   *
   * @package WordPress
   * @subpackage boilerplate
   */

  /*
   * Receives mixed[] $args Array structure from invoking template.
   * Default arguments included.
   */
  $args = wp_parse_args($args, [
    "gsap-delay" => 250,
    "index" => 0,
    "link" => "",
    "type" => "linkedin",
  ]);

  // Constructs GSAP delay based on provided arguments.
  $gsap_delay = $args["gsap-delay"] + ($args["gsap-delay"] * $args["index"]);

?>

<?php if ( !empty($args['link']) ) : ?>
  <div class="social__item" data-gsap="fade-up" data-gsap-delay="<?php echo esc_attr($gsap_delay); ?>" data-gsap-duration="750">
    <a class="social__link link" href="<?php echo esc_url($args['link']); ?>" target="_blank" title="Follow us">
      <?php get_template_part( "partials/svg/icon.{$args['type']}" ); ?>
    </a>
  </div>
<?php endif; ?>

