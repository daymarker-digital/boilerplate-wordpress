<?php

  /**
   * The template for the footer.
   *
   * @package WordPress
   * @subpackage boilerplate
   */

  // Partial data.
  $partial_name = "footer";
  $partial_classes = $partial_name;
  $partial_id = $partial_name;

  // Content (ACF) data.
  $footer_options = get_field( "footer", "options" ) ?: [];
  $footer_container = $footer_options["container"] ?? "container-fluid";
  $footer_navigation = $footer_options["navigation"] ?? [];

?>

<style data-block-id="<?php echo esc_attr($partial_name); ?>">
  <?php
    get_template_part( "partials/element-styles", null, [
      "background_colour" => $footer["background_colour"] ?? "",
      "id" => $partial_id,
      "padding_bottom" => $footer["padding_bottom"] ?? 0,
      "padding_top" => $footer["padding_top"] ?? 0,
      "text_colour" => $footer["text_colour"] ?? "",
    ]);
  ?>
</style>

<footer class="<?php echo esc_attr($partial_classes); ?>" id="<?php echo esc_attr($partial_id); ?>">
  <div class="<?php echo esc_attr($partial_name); ?>__flex flex-<?php echo esc_attr($footer_container); ?>">
    <p>&copy; <?php echo esc_html(date("Y") . " " . get_bloginfo( "name" )); ?>. All Rights Reserved.</p>
  </div>
</footer>
