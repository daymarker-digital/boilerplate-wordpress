<?php

  /**
   * The template for an element's inline styles.
   *
   * @package WordPress
   * @subpackage tenaciousdigital
   */

  use function DD\Base\Tools\{get_block_id_from_block};

  $id = get_block_id_from_block($args);
  $background_colour = $args["backgroundColor"] ?? "white";
  $padding_bottom = $args["style"]["spacing"]["padding"]["bottom"] ?? "0px";
  $padding_top = $args["style"]["spacing"]["padding"]["top"] ?? "0px";
  $text_colour = $args["textColor"] ?? "black";

?>

<?php if ( ! empty($id) ) : ?>
  #<?php echo esc_html($id); ?> {
    <?php if ( "none" !== $background_colour ) : ?>
      background: var(--wp--preset--color--<?php echo esc_html($background_colour); ?>);
    <?php endif; ?>
    <?php if ( "none" !== $text_colour ) : ?>
      color: var(--wp--preset--color--<?php echo esc_html($text_colour); ?>);
    <?php endif; ?>
    <?php if ( "initial" !== $padding_top ) : ?>
      padding-top: calc(<?php echo esc_html($padding_top); ?> * 0.666);
    <?php endif; ?>
    <?php if ( "initial" !== $padding_bottom ) : ?>
      padding-bottom: calc(<?php echo esc_html($padding_bottom); ?>  * 0.666);
    <?php endif; ?>
  }
  @media screen and (min-width: 992px) {
    #<?php echo esc_html($id); ?> {
      <?php if ( "initial" !== $padding_top ) : ?>
        padding-top: <?php echo esc_html($padding_top); ?>;
      <?php endif; ?>
      <?php if ( "initial" !== $padding_bottom ) : ?>
        padding-bottom: <?php echo esc_html($padding_bottom); ?>;
      <?php endif; ?>
    }
  }
<?php endif; ?>
