<?php

  /**
   * The template for the mobile menu drawer.
   *
   * @package WordPress
   * @subpackage boilerplate
   */

  // Partial data.
  $partial_name = "drawer-mobile-menu";
  $partial_classes = $partial_name;
  $partial_id = $partial_name;

  // Content (ACF) data.
  $mobile_menu_options = get_field( "mobile_menu", "options" ) ?: [];
  $navigation = $mobile_menu_options["navigation"] ?? [];
  $background_colour = $mobile_menu_options["background_colour"] ?? "green";
  $logo_colour = $mobile_menu_options["logo_colour"] ?? "sand";
  $text_colour = $mobile_menu_options["text_colour"] ?? "cloud";

?>

<div class="<?php echo esc_attr($partial_classes); ?>" id="<?php echo esc_attr($partial_id); ?>">
  <div class="<?php echo esc_attr($partial_classes); ?>__main">

    <nav role="navigation" aria-label="Header Menu">
      <!-- Navigation here... -->
    </nav>

  </div>
</div>
