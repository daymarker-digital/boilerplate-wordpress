<?php

  /**
   * The template for link.
   *
   * @package WordPress
   * @subpackage boilerplate
   */

  /*
   * Receives mixed[] $args Array structure from invoking template.
   * Default arguments included.
   */
  $args = wp_parse_args($args, [
    "link_appearance" => "link",
    "link_active" => false,
    "link_email" => "",
    "link_external" => "",
    "link_file" => "",
    "link_internal" => "",
    "link_page" => 0,
    "link_phone" => "",
    "link_style" => "primary",
    "link_target" => "_blank",
    "link_title" => "",
    "link_type" => "",
  ]);

  // Conditionally constructs $link based on "link_type".
  switch ( $args["link_type"] ) {
    case "external" : {
      $link = $args["link_external"];
      break;
    }
    case "email" : {
      $link = $args["link_email"] ? "mailto:{$args["link_email"]}" : "";
      break;
    }
    case "file" : {
      $link = $args["link_file"];
      break;
    }
    case "internal" : {
      $link = $args["link_internal"];
      $args["link_target"] = "_self";
      break;
    }
    case "page" : {
      $link = get_permalink($args["link_page"]);
      $args["link_target"] = "_self";
      break;
    }
    case "phone" : {
      $link = $args["link_phone"] ? "tel:" . str_replace( ".", "-", $args["link_phone"]) : "";
      break;
    }
    default: {
      $link = "";
      break;
    }
  }

  // Conditionally constructs $classes based on provided arguments.
  $classes = "{$args["link_appearance"]} {$args["link_appearance"]}--{$args["link_style"]}";

?>

<?php if ( !empty($args["link_title"]) && !empty($link) ) : ?>
  <a
    class="<?php echo esc_attr($classes); ?>"
    href="<?php echo esc_url($link); ?>"
    target="<?php echo esc_attr($args["link_target"]); ?>"
    title="<?php echo esc_attr($args["link_title"]); ?>"
  >
    <span class="<?php echo esc_attr($args["link_appearance"]); ?>__title"><?php echo esc_html($args["link_title"]); ?></span>
  </a>
<?php endif; ?>
