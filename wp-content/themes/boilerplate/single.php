<?php

  /**
   * Template Name: Single
   * Filename: single.php
   *
   * The main template for posts.
   *
   * @package WordPress
   * @subpackage boilerplate
   */

  get_header();

  if ( have_posts() ) {
    while ( have_posts() ) {

      the_post();

      the_content();

    }
  }

  get_footer();
