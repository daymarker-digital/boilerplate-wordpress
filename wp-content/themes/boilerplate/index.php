<?php

  /**
   * Template Name: Index
   * Filename: index.php
   *
   * The main template for the index.
   *
   * @package WordPress
   * @subpackage boilerplate
   */

  get_header();

  if ( have_posts() ) {
    while ( have_posts() ) {

      the_post();

      the_content();

    }
  }

  get_footer();
