<?php

  /**
   * The template for an image block.
   *
   * @package WordPress
   * @subpackage tenaciousdigital
   */

  use function DD\Base\Tools\unique_id;

  // Block data
  $block_name = "image";
  $block_classes = "{$block_name} block block--{$block_name}";
  $block_data = $block["data"] ?? [];
  $block_id = unique_id("{$block_name}--");

  // Content (ACF) data
  $aspect_ratio_mobile = get_field("aspect_ratio_mobile") ?: "";
  $container = get_field("container") ?: "container";
  $show = get_field("show");

  // ---------------------------------------- Conditional
  $block_classes .= " mobile-aspect-ratio--" . ( $aspect_ratio_mobile ? "enabled" : "disabled" );

?>

<?php if ( $show ) : ?>

  <style data-block-id="<?php echo esc_attr($block_name); ?>">

    <?php
      get_template_part( "partials/element-styles", null, [
        "background_colour" => get_field("background_colour"),
        "id" => $block_id,
        "padding_bottom" => get_field("padding_bottom"),
        "padding_top" => get_field("padding_top"),
        "text_colour" => get_field("text_colour"),
      ]);
    ?>

    <?php if ( !empty($aspect_ratio_mobile) ) : ?>
      #<?php echo esc_html( $block_id ); ?> figure {
        aspect-ratio: <?php echo esc_html($aspect_ratio_mobile); ?>;
      }
      @media screen and (min-width: 992px) {
        #<?php echo esc_html( $block_id ); ?> figure {
          aspect-ratio: initial;
        }
      }
    <?php endif; ?>

  </style>

  <section class="<?php echo esc_attr($block_classes); ?>" id="<?php echo esc_attr($block_id); ?>">
    <div class="<?php echo esc_attr($block_name); ?>__grid grid-<?php echo esc_attr($container); ?>">
      <div class="<?php echo esc_attr($block_name); ?>__content">
        <figure class="<?php echo esc_attr($block_name); ?>__image">
          <?php get_template_part( "partials/lazyload/image", null, [ "image" => get_field("image") ] ); ?>
        </figure>
      </div>
    </div>
  </section>

<?php endif; ?>
