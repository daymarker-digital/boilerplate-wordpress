<?php

  /**
   * The template for a carousel block.
   *
   * @package WordPress
   * @subpackage tenaciousdigital
   */

  use function DD\Base\Tools\unique_id;

  // Block data
  $block_name = "carousel";
  $block_classes = "{$block_name} block block--{$block_name}";
  $block_data = $block["data"] ?? [];
  $block_id = unique_id("{$block_name}--");

  // Content (ACF) data
  $container = get_field("container") ?: "full-width";
  $autoplay = get_field("autoplay");
  $rewind = get_field("rewind");
  $limit = get_field("limit");
  $gap = get_field("gap");
  $interval = get_field("interval");
  $speed = get_field("speed");
  $items = get_field("items") ?: [];
  $show = get_field("show");
  $heading = get_field("heading");

?>

<?php if ( $show ) : ?>

  <style data-block-id="<?php echo esc_attr($block_name); ?>">
    <?php
      get_template_part( "partials/element-styles", null, [
        "background_colour" => get_field("background_colour"),
        "id" => $block_id,
        "padding_bottom" => get_field("padding_bottom"),
        "padding_top" => get_field("padding_top"),
        "text_colour" => get_field("text_colour"),
      ]);
    ?>
  </style>

  <section class="<?php echo esc_attr($block_classes); ?>" id="<?php echo esc_attr($block_id); ?>">
    <div class="<?php echo esc_attr($block_name); ?>__grid grid-<?php echo esc_attr($container); ?>">
      <div class="<?php echo esc_attr($block_name); ?>__content" data-gsap-scroll-trigger-group>

        <?php if ( !empty($heading) ) : ?>
          <h2
            class="<?php echo esc_attr($block_name); ?>__heading heading--tertiary heading--block-xs"
            data-gsap="fade-left"
            data-gsap-delay="0"
            data-gsap-duration="750"
          ><?php echo esc_html($heading); ?></h2>
        <?php endif; ?>

        <?php if ( !empty($items) ) : ?>
          <div
            class="<?php echo esc_attr($block_name); ?>__items splide"
            data-carousel-autoplay="<?php echo esc_attr($autoplay); ?>"
            data-carousel-gap="<?php echo esc_attr($gap); ?>"
            data-carousel-interval="<?php echo esc_attr($interval); ?>"
            data-carousel-rewind="<?php echo esc_attr($rewind); ?>"
            data-carousel-speed="<?php echo esc_attr($speed); ?>"
            data-carousel-style="<?php echo esc_attr($block_name); ?>"
            data-gsap="fade-up"
            data-gsap-delay="250"
            data-gsap-duration="750"
          >
            <div class="splide__track">
              <ul class="splide__list" role=listbox>
                <?php foreach( $items as $item ) : ?>
                  <?php if ( isset($item["image"]) && !empty($item["image"]) ) : ?>
                    <li class="splide__slide" role="option">
                      <div class="splide__content">
                        <?php get_template_part( "partials/lazyload/image", null, [ "image" => $item["image"] ] ); ?>
                      </div>
                    </li>
                  <?php endif; ?>
                <?php endforeach; ?>
              </ul>
            </div>
          </div>
        <?php endif; ?>

      </div>
    </div>
  </section>

<?php endif; ?>
