<?php

  /**
   * The template for a fifty-fifty block.
   *
   * @package WordPress
   * @subpackage tenaciousdigital
   */

  use function DD\Base\Tools\{get_block_id_from_block, debug_this, unique_id};

  // Block data
  $block_name = "fifty-fifty";
  $block_classes = "{$block_name} block block--{$block_name}";
  $block_id = get_block_id_from_block($block);

  // ACF data
  $content = get_field("content") ?: [];
  $style = get_field("style") ?: "wide";

?>

<?php if ( get_field("enable") ) : ?>

  <section class="<?php echo esc_attr($block_classes); ?>" id="<?php echo esc_attr($block_id); ?>" data-gsap-scroll-trigger>

    <style data-block-id="<?php echo esc_attr($block_name); ?>">
      <?php get_template_part( "partials/block-styles", null, $block); ?>
    </style>

    <div class="<?php echo esc_attr($block_name); ?>__grid grid-container">

      <?php if ( ! empty($content) ) : ?>
        <div class="<?php echo esc_attr($block_name); ?>__layout <?php echo esc_attr($style); ?>">
          <?php foreach ( $content as $column ) : ?>

            <?php $content_type = $column["acf_fc_layout"] ?? ""; ?>

            <div class="<?php echo esc_attr($block_name); ?>__column <?php echo esc_attr($content_type); ?>">
              <div class="<?php echo esc_attr($block_name); ?>__<?php echo esc_attr($content_type); ?>">
                <?php
                  switch ( $content_type ) {
                    case "image": {
                      get_template_part("partials/content/image", null, $column["image"]);
                      break;
                    }
                    case "text": {
                      $heading = [
                        "content" => $column["heading"] ?? "",
                        "classes" => [ "fifty-fifty__heading", "heading--primary" ],
                      ];
                      get_template_part("partials/content/heading", null, $heading);
                      $message = [
                        "content" => $column["message"] ?? "",
                        "classes" => [ "fifty-fifty__message", "body-copy--primary" ],
                      ];
                      get_template_part("partials/content/message", null, $message);
                      break;
                    }
                    default: {
                      get_template_part("partials/missing-content", null, []);
                      break;
                    }
                  }
                ?>
              </div>
            </div>
          <?php endforeach; ?>
        </div>
      <?php endif; ?>

    </div>
  </section>

<?php endif; ?>
