<?php

  /**
   * The template for a hero block.
   *
   * @package WordPress
   * @subpackage tenaciousdigital
   */

  use function DD\Base\Tools\{debug_this, get_block_id_from_block, unique_id};

  // Block data
  $block_name = "hero";
  $block_classes = "{$block_name} block block--{$block_name}";
  $block_id = get_block_id_from_block($block);

  // ACF Data
  $background_image = get_field("background_image") ?: [];
  $cta = get_field("cta") ?: [];
  $cta["classes"] = [ "button--primary", "button--orange" ];

?>

<?php if ( get_field("enable") ) : ?>

  <section class="<?php echo esc_attr($block_classes); ?>" id="<?php echo esc_attr($block_id); ?>" data-gsap-scroll-trigger>

    <style data-block-id="<?php echo esc_attr($block_name); ?>">
      <?php get_template_part( "partials/block-styles", null, $block); ?>
    </style>

    <?php if ( ! empty($background_image) ) : ?>
      <div class="<?php echo esc_attr($block_name); ?>__background">
        <?php get_template_part("partials/content/image", null, $background_image); ?>
      </div>
      <div class="<?php echo esc_attr($block_name); ?>__overlay"></div>
    <?php endif; ?>

    <div class="<?php echo esc_attr($block_name); ?>__grid grid-container">
      <div class="<?php echo esc_attr($block_name); ?>__content">
        <h2 class="<?php echo esc_attr($block_name); ?>__heading heading--primary heading--1">Data-driven decision-making through best practice analytics solutions.</h2>
        <?php if ( ! empty($cta) ) : ?>
          <div class="<?php echo esc_attr($block_name); ?>__cta">
            <?php get_template_part( "partials/content/link", null, $cta); ?>
          </div>
        <?php endif; ?>
      </div>
    </div>

  </section>

<?php endif; ?>
