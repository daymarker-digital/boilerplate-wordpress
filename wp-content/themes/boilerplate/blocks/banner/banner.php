<?php

  /**
   * The template for a banner block.
   *
   * @package WordPress
   * @subpackage tenaciousdigital
   */

  use function DD\Base\Tools\{get_block_id_from_block, debug_this, unique_id};

  // Block data
  $block_name = "banner";
  $block_classes = "{$block_name} block block--{$block_name}";
  $block_data = $block["data"] ?? [];
  $block_id = get_block_id_from_block($block);

  // ACF data
  $content = get_field("content") ?: "";

?>

<?php if ( get_field("enable") ) : ?>

  <section class="<?php echo esc_attr($block_classes); ?>" id="<?php echo esc_attr($block_id); ?>" data-gsap-scroll-trigger>

    <style data-block-id="<?php echo esc_attr($block_name); ?>">
      <?php get_template_part( "partials/block-styles", null, $block); ?>
    </style>

    <?php if ( ! empty($content) ) : ?>
      <strong class="<?php echo esc_attr($block_name); ?>__content heading--primary heading--callout"><?php echo wp_kses_post($content); ?></strong>
    <?php endif; ?>

  </section>

<?php endif; ?>
