<?php

  /**
   * The template for an Instagram feed block.
   *
   * @package WordPress
   * @subpackage boilerplate
   */

  use function DD\Base\Tools\unique_id;

  // Block data
  $block_name = "instagram-feed";
  $block_classes = "{$block_name} block block--{$block_name}";
  $block_data = $block["data"] ?? [];
  $block_id = unique_id("{$block_name}--");

  // Content (ACF) data
  $show = get_field("show");
  $feed_account = get_field("feed_account") ?: "brendenmersey";
  $feed_limit = get_field("feed_limit") ?: 10;

?>

<?php if ( $show ) : ?>

  <style data-block-id="<?php echo esc_attr($block_name); ?>">
    <?php
      get_template_part( "partials/element-styles", null, [
        "background_colour" => get_field("background_colour"),
        "id" => $block_id,
        "padding_bottom" => get_field("padding_bottom"),
        "padding_top" => get_field("padding_top"),
        "text_colour" => get_field("text_colour"),
      ]);
    ?>
  </style>

  <section class="<?php echo esc_attr($block_classes); ?>" id="<?php echo esc_attr($block_id); ?>">
    <div class="<?php echo esc_attr($block_name); ?>__grid grid-container">
      <div class="<?php echo esc_attr($block_name); ?>__content">
        <div class="<?php echo esc_attr($block_name); ?>__grid js--instagram-feed" data-instagram-feed-account="<?php echo esc_attr($feed_account); ?>" data-instagram-feed-limit="<?php echo esc_attr($feed_limit); ?>">
          <?php for ( $i = 0; $i < $feed_limit; $i++ ) : ?>
            <div class="<?php echo esc_attr($block_name); ?>__item" data-index="<?php echo esc_attr($i); ?>">
              <a class="<?php echo esc_attr($block_name); ?>__item-link" href="" target="_blank">
                <div class="<?php echo esc_attr($block_name); ?>__item-image" data-bg=""></div>
              </a>
            </div>
          <?php endfor; ?>
        </div>
      </div>
    </div>
  </section>

<?php endif; ?>
