/**
 * Cookies module.
 *
 * @module cookies
 * @version 1.0
 */

/**
 * Config object.
 *
 * Stores module information.
 *
 * @type {Object}
 */
const CONFIG = {
  debug: false,
  name: "cookies.js",
  version: "1.0",
};

/**
 * Set cookie.
 *
 * Return an options object that can take optional settings
 * that will override the default settings where needed.
 *
 * @param {Object} of custom carousel options
 * @returns {Object} of carousel options
 */
function set(name = "", value = "", expires = 1) {
  const d = new Date();
  d.setTime(d.getTime() + expires * 24 * 60 * 60 * 1000);
  expires = "expires=" + d.toUTCString();
  document.cookie = name + "=" + value + ";" + expires + ";path=/";
}

/**
 * Get cookie.
 *
 * Return an options object that can take optional settings
 * that will override the default settings where needed.
 *
 * @param {Object} of custom carousel options
 * @returns {Object} of carousel options
 */
function get(name = "") {
  name = name + "=";
  let ca = document.cookie.split(";");

  for (let i = 0; i < ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) == " ") {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }

  return "";
}

export default { get, set };
