export default {
  startColor: "#57FF00",
  endColor: "red",
  fontSize: "16px",
  fontWeight: "300",
  indent: 24,
};
