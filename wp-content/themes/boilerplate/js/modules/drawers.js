import Tools from "tools";

/**
 * Drawers module.
 *
 * @module drawers
 * @version 1.0
 */

/**
 * Config object.
 *
 * Stores module information.
 *
 * @type {Object}
 */
const config = {
  debug: false,
  name: "drawers.js",
  version: "1.0",
};

/**
 * Target HTML elements.
 *
 * Cached collection of HTML elements to be that will have classes
 * have classes added to.
 *
 * @type {HTMLElement}
 */
const elements = document.querySelectorAll("body, footer, header, main") || [];

/**
 * Trigger HTML elements.
 *
 * Cached collection of HTML elements that will have event listeners added to.
 *
 * @type {HTMLElement}
 */
const triggers = document.querySelectorAll(".js--toggle-drawer") || [];

/**
 * Drawers object.
 *
 * Stores every instance of drawer created on page in object.
 *
 * @type {Object}
 */
const drawers = {};

/**
 * Open drawer by ID.
 *
 * Open drawer by ID by adding a class to the cached
 * elements. Optionally include a delay in milliseconds.
 *
 * @param {string} id - The element id of the target drawer.
 * @param {number} delay - Optional delay before adding class to elements.
 */
const openDrawerByID = (id = "", delay = 0) => {
  setTimeout(() => {
    Tools.addClass(`${id}--active`, elements);
  }, delay);
};

/**
 * Toggle drawer by ID.
 *
 * Toggle drawer by ID by adding a class to the cached
 * elements. Optionally include a delay in milliseconds.
 *
 * @param {string} id - The element id of the target drawer.
 * @param {number} delay - Optional delay before adding class to elements.
 */
const toggleDrawerByID = (id = "", delay = 0) => {
  setTimeout(() => {
    Tools.toggleClass(`${id}--active`, elements);
  }, delay);
};

/**
 * Close drawer by ID.
 *
 * Close drawer by ID by adding a class to the cached
 * elements. Optionally include a delay in milliseconds.
 *
 * @param {string} id - The element id of the target drawer.
 * @param {number} delay - Optional delay before adding class to elements.
 */
const closeDrawerByID = (id = "", delay = 0) => {
  setTimeout(() => {
    Tools.removeClass(`${id}--active`, elements);
  }, delay);
};

/**
 * On click, open drawer from ID.
 *
 * On button click, get target drawer ID and open drawer
 * by invoking toggleDrawerByID(). Update drawers object.
 */
const onClickOpenDrawerFromID = () => {
  (document.querySelectorAll(".js--open-drawer") || []).forEach((button) => {
    let drawerID = button.dataset?.drawerId || "";
    button.addEventListener("click", (event) => {
      openDrawerByID(drawerID);
      drawers[drawerID] = true;
    });
  });
};

/**
 * On click, toggle drawer from ID.
 *
 * On button click, get target drawer ID and toggle drawer
 * by invoking toggleDrawerByID(). Update drawers object.
 */
const onClickToggleDrawerFromID = () => {
  triggers.forEach((trigger) => {
    let drawerID = trigger.dataset?.drawerId || "";
    trigger.addEventListener("click", (event) => {
      toggleDrawerByID(drawerID);
      drawers[drawerID] = drawers?.[drawerID] ? false : true;
    });
  });
};

/**
 * On click, close all drawers.
 *
 * On button click, iterate through drawers object and
 * close each instance by invoking closeDrawerByID();
 */
const onClickCloseAllDrawers = () => {
  document.body.addEventListener("click", (event) => {
    let drawer = event.target.closest(".drawer") ? true : false;
    let buttonDrawerOpen = event.target.closest(".js--open-drawer") ? true : false;
    let buttonDrawerClose = event.target.closest(".js--close-drawer") ? true : false;
    let buttonDrawerToggle = event.target.closest(".js--toggle-drawer") ? true : false;
    if ((!drawer && !buttonDrawerOpen && !buttonDrawerToggle) || buttonDrawerClose) {
      for (const id in drawers) {
        if (drawers[id]) {
          closeDrawerByID(id);
          drawers[id] = false;
        }
      }
    }
  });
};

/**
 * Initialize module.
 */
const init = () => {
  if (config.debug) console.log(`[ ${config.name} v.${config.version} initialized ]`);
  onClickToggleDrawerFromID();
  onClickCloseAllDrawers();
  if (config.debug) console.log(`[ ${config.name} v.${config.version} complete ]`);
};

export default { closeDrawerByID, init, openDrawerByID };
