/**
 * Scroll states module.
 *
 * @module scrollStates
 * @version 1.0
 */

/**
 * Config object.
 *
 * Stores module information.
 *
 * @type {Object}
 */
const CONFIG = {
  debug: false,
  name: "scrollStates.js",
  version: "1.0",
};

/**
 * Target HTML elements.
 *
 * Cached collection of HTML elements to be
 * used in the module.
 *
 * @type {Object}
 */
const ELEMENTS = document.querySelectorAll("body, footer, header, main") || [];

/**
 * Classes.
 *
 * Cached collection of classes to be
 * used in the module.
 *
 * @type {Object}
 */
const CLASSES = {
  atTop: "scroll-position--at-top",
  scrolled: "scroll-position--scrolled",
  scrollingDown: "scroll-position--scrolling-down",
  scrollingUp: "scroll-position--scrolling-up",
};

/**
 * Scroll position object.
 *
 * Cached collection of scroll position information.
 *
 * @type {Object}
 */
const SCROLL_POSITION = {
  current: window.scrollY || document.documentElement.scrollTop,
  previous: window.scrollY || document.documentElement.scrollTop,
  down: false,
};

/**
 * Set classes.
 *
 * Sets the classes for the scrolling module.
 *
 * @returns {void}
 */
function setClasses() {
  const scrolled = SCROLL_POSITION.current > 30;
  const atTop = SCROLL_POSITION.current <= 0;
  const down = SCROLL_POSITION.current > SCROLL_POSITION.previous ? true : false;

  ELEMENTS.forEach((element) => {
    element.classList[scrolled ? "add" : "remove"](CLASSES.scrolled);
    element.classList[atTop ? "add" : "remove"](CLASSES.atTop);
    element.classList[down ? "add" : "remove"](CLASSES.scrollingDown);
    element.classList[down ? "remove" : "add"](CLASSES.scrollingUp);
  });
}

/**
 * Set initial scroll position.
 *
 * Sets the initial scroll position for the scrolling module.
 *
 * @param {Object} smoother - The smoother object.
 * @returns {void}
 */
function setInitialScrollPosition(smoother = false) {
  SCROLL_POSITION.current = smoother ? smoother.scrollTop() : window.scrollY;
  SCROLL_POSITION.previous = smoother ? smoother.scrollTop() : window.scrollY;
}

/**
 * Update scroll position.
 *
 * Updates the scroll position for the scrolling module.
 *
 * @param {Object} smoother - The smoother object.
 * @returns {void}
 */
function updateScrollPosition(smoother = false) {
  SCROLL_POSITION.previous = SCROLL_POSITION.current;
  SCROLL_POSITION.current = smoother ? smoother.scrollTop() : window.scrollY;
}

/**
 * Refresh module.
 */
function refresh(smoother = false) {
  if (CONFIG.debug) console.log(`[ ${CONFIG.name} v.${CONFIG.version} refreshed ]`);
  updateScrollPosition(smoother);
  setClasses();
  if (CONFIG.debug) console.log(`[ ${CONFIG.name} v.${CONFIG.version} complete ]`);
}

/**
 * Initialize module.
 */
function init(smoother = false) {
  if (CONFIG.debug) console.log(`[ ${CONFIG.name} v.${CONFIG.version} initialized ]`);
  setInitialScrollPosition(smoother);
  setClasses();
  if (CONFIG.debug) console.log(`[ ${CONFIG.name} v.${CONFIG.version} complete ]`);
}

export default { init, refresh };
