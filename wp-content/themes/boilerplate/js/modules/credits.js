/**
 * Credits module.
 *
 * @module credits
 * @version 1.0
 */

/**
 * Config object.
 *
 * Stores module information.
 *
 * @type {Object}
 */
const CONFIG = {
  debug: false,
  name: "credits.js",
  version: "1.0",
};

const CREDITS = {
  company: "Boilerplate",
  tagline: "This is a Boilerplate",
  version: "1.0",
};

/**
 * Initialize module.
 */
function init() {
  if (CONFIG.debug) console.log(`[ ${CONFIG.name} v.${CONFIG.version} initialized ]`);
  console.log(`${CREDITS.company} - "${CREDITS.tagline}" - Version ${CREDITS.version}`);
  console.log("Site by Daymarker Digital – https://daymarker.digital");
  if (CONFIG.debug) console.log(`[ ${CONFIG.name} v.${CONFIG.version} complete ]`);
}

export default { init };
