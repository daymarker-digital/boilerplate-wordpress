import Animations from "animations";

/**
 * Carousels module.
 *
 * @module carousels
 * @version 1.0
 */

/**
 * Config object.
 *
 * Stores module information.
 *
 * @type {Object}
 */
const config = {
  debug: false,
  name: "carousels.js",
  version: "1.0",
};

/**
 * Target HTML elements.
 *
 * Cached collection of HTML elements to be that will be used
 * to initialize Splide.js
 *
 * @type {HTMLElement}
 */
const elements = document.querySelectorAll(".splide") || [];

/**
 * Carousels object.
 *
 * Stores every instance of carousel created on page in object.
 *
 * @type {Object}
 */
const carousels = {};

/**
 * Carousel options factory function.
 *
 * Return an options object that can take optional settings
 * that will override the default settings where needed.
 *
 * @param {Object} of custom carousel options
 * @returns {Object} of carousel options
 */
const optionsFactory = (custom = {}) => {
  let standard = {
    arrows: false,
    autoplay: true,
    interval: 3250,
    mediaQuery: "min",
    pagination: false,
    perMove: 1,
    speed: 1000,
  };

  return { ...standard, ...custom };
};

/**
 * Mount Splide.js from HTLM element.
 *
 * Mount instance of Splide.js with settings derrived from HTML
 * data attributes and the options factory functions.
 *
 * @param {HTMLElement} of an HTML element with Splide.js markup
 */
const mountSplide = (element = false) => {
  if (element) {
    let autoplay = parseInt(element.dataset?.carouselAutoplay || 0);
    let elementID = element.id || "not-set";
    let gap = parseInt(element.dataset?.carouselGap || 0);
    let interval = parseInt(element.dataset?.carouselInterval || 2500);
    let rewind = parseInt(element.dataset?.carouselRewind || 0); // boolean
    let speed = parseInt(element.dataset?.carouselSpeed || 1000);
    let style = element.dataset?.carouselStyle || "style-not-set";
    let options = optionsFactory({
      autoplay,
      gap,
      interval,
      rewind,
      speed,
    });

    switch (style) {
      case "client-listing": {
        options.type = "loop";
        options.perPage = 2;
        options.perMove = 1;
        options.padding = {
          left: "15%",
          right: "15%",
        };
        options.breakpoints = {
          1400: {
            perPage: 6,
            padding: {
              left: "15%",
              right: "15%",
            },
          },
          1200: {
            perPage: 5,
            padding: {
              left: "15%",
              right: "15%",
            },
          },
          992: {
            perPage: 4,
            padding: {
              left: "15%",
              right: "15%",
            },
          },
          768: {
            perPage: 4,
            padding: {
              left: "15%",
              right: "15%",
            },
          },
          576: {
            perPage: 4,
            padding: {
              left: "15%",
              right: "15%",
            },
          },
        };
        break;
      }
      case "instagram-feed": {
        options.perPage = 2;
        options.breakpoints = {
          1400: {
            perPage: 6,
          },
          1200: {
            perPage: 5,
          },
          992: {
            perPage: 4,
          },
          768: {
            perPage: 4,
          },
          576: {
            perPage: 3,
          },
        };
        break;
      }
      case "latest-articles": {
        options.breakpoints = {
          1200: {
            perPage: 4,
            padding: {
              left: "0%",
              right: "20%",
            },
          },
          768: {
            perPage: 3,
            padding: {
              left: "0%",
              right: "25%",
            },
          },
          768: {
            perPage: 3,
            padding: {
              left: "0%",
              right: "20%",
            },
          },
          576: {
            perPage: 2,
            padding: {
              left: "0%",
              right: "20%",
            },
          },
        };
        options.padding = {
          left: "0%",
          right: "20%",
        };
        options.perPage = 1;
        options.type = "loop";
        break;
      }
    }

    let splide = new Splide(element, options);

    splide.on("mounted move resized", (event) => {
      Animations.init();
    });

    splide.on("refresh", (event) => {});

    splide.mount();

    (document.querySelectorAll(`[data-target-id="${elementID}"].next`) || []).forEach((button) => {
      button.addEventListener("click", (event) => {
        splide.go(">");
      });
    });

    (document.querySelectorAll(`[data-target-id="${elementID}"].prev`) || []).forEach((button) => {
      button.addEventListener("click", (event) => {
        splide.go("<");
      });
    });

    carousels[elementID] = splide;
  }
};

/**
 * Refresh all instances within the carousel object.
 *
 * Loop over each property within the carousel object and
 * refreshes the Splide instance.
 */
const refresh = () => {
  for (const id in carousels) {
    carousels[id].refresh();
  }
};

/**
 * Initialize module.
 */
const init = () => {
  if (config.debug) console.log(`[ ${config.name} v.${config.version} initialized ]`);
  elements.forEach((element) => mountSplide(element));
  if (config.debug) console.log(`[ ${config.name} v.${config.version} complete ]`);
};

export default { init, refresh };
