import validator from "validator";

/**
 * Forms module.
 *
 * @module forms
 * @version 1.0
 */

/**
 * Config object.
 *
 * Stores module information.
 *
 * @type {Object}
 */
const config = {
  debug: false,
  name: "forms.js",
  version: "1.0",
};

/**
 * Target HTML elements.
 *
 * Cached collection of HTML form elements to be
 * validated on the client side.
 *
 * @type {HTMLElement}
 */
const forms = document.querySelectorAll("form.js--validate-me") || [];

/**
 * Array of form files.
 *
 * Stores all files per form.
 *
 * @type {Array}
 */
const formFiles = [];

/**
 * Form file size limit.
 *
 * Max file size for each file in the form.
 *
 * @type {Number}
 */
const maxMB = 25.0;

/**
 * reCaptcha object.
 *
 * Stores reCaptcha information.
 *
 * @type {Object}
 */
const reCaptcha = {
  enable: false,
  v3: {
    keys: {
      site: "",
    },
  },
};

/**
 * Check if field element is valid.
 *
 * @param {HTMLElement} field - The form field to be validated.
 * @returns {Boolean} true if valid.
 */
const isFieldValid = (field = false) => {
  if (config.debug) console.log("[ isFieldValid() initialized ]", field);

  switch (field.nodeName) {
    case "INPUT": {
      return isInputValid(field);
    }
    case "SELECT": {
      alert("forms.js :: select element form validation not configured!");
      break;
    }
    case "TEXTAREA": {
      return isTextareaValid(field);
    }
    default: {
      return false;
    }
  }
};

/**
 * Check if form element is valid.
 *
 * @param {HTMLElement} form - The form to be validated.
 * @returns {Boolean} true if valid.
 */
const isFormValid = (form = false) => {
  const requiredFields = form.querySelectorAll(".required") || [];
  const requiredFieldsCount = requiredFields.length;
  let validFieldsCount = 0;

  requiredFields.forEach((field) => {
    if (isFieldValid(field)) validFieldsCount++;
  });

  return requiredFieldsCount == validFieldsCount ? true : false;
};

/**
 * Check if input element is valid.
 *
 * @param {HTMLElement} input - The input to be validated.
 * @returns {Boolean} true if valid.
 */
const isInputValid = (input = {}) => {
  let { type = "", valid = false, value = "" } = input;

  switch (type) {
    case "email": {
      valid = validator.isEmail(value);
      break;
    }
    case "tel": {
      valid = validator.isMobilePhone(value, ["en-CA", "en-US"]);
      break;
    }
    case "text": {
      valid = !validator.isEmpty(value);
      break;
    }
  }

  updateFieldState(input, valid);

  if (config.debug) console.log("[ isInputValid() complete ]", { type, value });

  return valid;
};

/**
 * Check if textarea element is valid.
 *
 * @param {HTMLElement} textarea - The input to be validated.
 * @returns {Boolean} true if valid.
 */
const isTextareaValid = (textarea = {}) => {
  let { value = "" } = textarea;
  let valid = !validator.isEmpty(value);

  updateFieldState(textarea, valid);

  if (config.debug) console.log("[ isTextareaValid() complete ]", { value, valid });

  return valid;
};

/**
 * Add focus based event listeners to form.
 *
 * @param {HTMLElement} form - The form to attach listeners to.
 */
const addFocusListenersToForm = (form = false) => {
  if (form) {
    form.addEventListener("focusin", (e) => {
      let field = e.target.closest(".field") || false;
      if (field) field.classList.add("in-focus");
    });
    form.addEventListener("focusout", (e) => {
      let field = e.target.closest(".field") || false;
      if (field && !e.target.value) {
        isFieldValid(field);
        field.classList.remove("in-focus");
      }
    });
  }
};

/**
 * Submit form with async/await.
 *
 * Submit and post form data using async/await.
 *
 * @param {String} url - The url of the form action.
 * @param {Object} data - The form data object to be posted.
 * @returns {Promise}
 */
const asyncSubmitFormData = async (url = "", data = {}) => {
  try {
    return await axios({ method: "post", url, data });
  } catch (error) {
    console.log(error);
    throw error;
  }
};

/**
 * Update field state.
 *
 * Update field state with classes based on input validity.
 *
 * @param {HTMLElement} input - The input representing the field.
 * @param {Boolean} valid - The validity of input.
 */
const updateFieldState = (input = {}, valid = false) => {
  let field = input.closest(".form__field") || false;

  if (field) {
    if (valid) {
      field.classList.remove("error");
    } else {
      field.classList.add("error");
    }
  }

  if (config.debug) console.log("[ updateFieldState() ]", { field, valid });
};

/**
 * Update form notice.
 *
 * Update user by replacing the form with a success message
 *
 * @param {Object} response - The response data returned from the form submission.
 * @param {HTMLElement} form - The form that was recently used to submit data
 */
const updateFormNotice = (response = {}, form = {}) => {
  if (200 === response.status) {
    const successAction = form.dataset?.successAction || "none";

    form.innerHTML = `
      <div class="form__notice">
        <p class="form__success-message">Thanks for your interest!</p>
      </div>
    `;

    switch (successAction) {
      case "close-modal": {
        let modal = form.closest(".modal") || false;
        let modalID = modal.id || "id-not-set";
        if (modalID) setTimeout(() => MicroModal.close(modalID), 1750);
        break;
      }
    }
  }
};

/**
 * On click, handle submit event.
 *
 * @param {HTMLElement} form - The form to be handled.
 */
const onButtonClickHandleSubmit = (form = false) => {
  (form.querySelectorAll("button[type='submit']") || []).forEach((button) => {
    button.addEventListener("click", (event) => {
      event.preventDefault();

      if (isFormValid(form)) {
        const formData = new FormData(form);
        const formAction = form.action || "";
        formData.set("g-recaptcha-enabled", false);

        if (reCaptcha.enable) {
          grecaptcha.ready(() => {
            grecaptcha.execute(reCaptcha.v3.keys.site, { action: "submit" }).then((token) => {
              formData.set("g-recaptcha-response", token);
              formData.set("g-recaptcha-enabled", true);
              asyncSubmitFormData(formAction, formData)
                .then((response) => updateFormNotice(response, form))
                .catch(console.error);
            });
          });
        } else {
          asyncSubmitFormData(formAction, formData)
            .then((response) => updateFormNotice(response, form))
            .catch(console.error);
        }
      }
    });
  });
};

/**
 * Initialize module.
 */
const init = () => {
  if (config.debug) console.log(`[ ${config.name} v.${config.version} initialized ]`);
  forms.forEach((form) => {
    addFocusListenersToForm(form);
    onButtonClickHandleSubmit(form);
  });
  if (config.debug) console.log(`[ ${config.name} v.${config.version} complete ]`);
};

export default { init };
