import { getHeaderHeight } from "./tools";

/**
 * Animations module.
 *
 * @module animations
 * @version 1.0
 */

/**
 * Config object.
 *
 * Stores module information.
 *
 * @type {Object}
 */
const CONFIG = {
  debug: true,
  name: "animations.js",
  version: "1.0",
};

/**
 * Target HTML elements.
 *
 * Cached collection of HTML elements to be
 * used in the module.
 *
 * @type {HTMLElement}
 */
const ELEMENTS = {
  accordionButtons: document.querySelectorAll(".accordion__button") || [],
  backToTopButtons: document.querySelectorAll(".js--back-to-top") || [],
  navLinks: gsap.utils.toArray("nav a") || [],
  scrollTriggers: gsap.utils.toArray(`[data-gsap-scroll-trigger-type]`) || [],
};

/**
 * Target timelines.
 *
 * Cached collection of timelines to be
 * used in the module.
 *
 * @type {Object}
 */
const TIMELINES = {
  mobileMenu: gsap
    .timeline({ paused: true, reversed: true })
    .set(ELEMENTS.mobileMenu, {
      height: "var(--theme-viewport-height, 100vh)",
    })
    .to(ELEMENTS.mobileMenu, {
      duration: 0.5,
      y: 0,
      ease: "power3.out",
    }),
};

/**
 * Toggle accordion elements on click.
 */
function onClickToggleAccordion() {
  ELEMENTS.accordionButtons.forEach((button) => {
    // initialize isOpen state
    let isOpen = false;

    // handle click event
    button.addEventListener("click", () => {
      const accordionID = button.getAttribute("data-target-accordion") || "not-set";
      const accordion = document.getElementById(accordionID);

      if (accordion) {
        if (!isOpen) {
          gsap.to(accordion, {
            onStart: () => {
              button.classList.add("open");
              accordion.classList.add("open");
            },
            onComplete: () => {
              button.classList.add("opened");
              accordion.classList.add("opened");
            },
            height: "auto",
            duration: 0.333,
            ease: "ease",
            overwrite: true,
          });
          isOpen = true;
        } else {
          gsap.to(accordion, {
            onStart: () => {
              button.classList.remove("open");
              accordion.classList.remove("open");
            },
            onComplete: () => {
              button.classList.remove("opened");
              accordion.classList.remove("opened");
            },
            height: 0,
            overwrite: true,
          });
          isOpen = false;
        }
      }
    });
  });
}

/**
 * Toggle mobile menu on click.
 */
function onClickToggleMobileMenu() {
  ELEMENTS.mobileMenuToggleButtons.forEach((button) => {
    button.addEventListener("click", () => {
      TIMELINES.mobileMenu.reversed() ? TIMELINES.mobileMenu.play() : TIMELINES.mobileMenu.reverse();
      document.body.classList.toggle("mobile-menu--active");
    });
  });
}

/**
 * Scroll to hash on click.
 */
function onClickScrollToHash(smoother = false) {
  ELEMENTS.navLinks.forEach((link) => {
    link.addEventListener("click", (event) => {
      const targetUrl = new URL(link.href);
      const targetUrlNoHash = targetUrl.href.replace(targetUrl.hash, "");
      const currentUrlNoHash = window.location.href.replace(window.location.hash, "");
      const targetHash = targetUrl.hash;

      if (targetUrlNoHash === currentUrlNoHash) {
        event.preventDefault();
        scrollToHash(smoother, targetHash, 0.25);
        if (link.closest(".mobile-menu")) {
          TIMELINES.mobileMenu.reverse();
          document.body.classList.remove("mobile-menu--active");
        }
      }
    });
  });
}

/**
 * Handle click event on navigation links.
 */
function onClickScrollToTop(smoother = false) {
  ELEMENTS.backToTopButtons.forEach((button) => {
    button.addEventListener("click", () => {
      gsap.to(smoother, {
        scrollTop: 0,
        duration: 2,
        ease: "power3.out",
      });
    });
  });
}

/**
 * Animate element on scroll.
 */
function onScrollAnimateElement() {
  ELEMENTS.scrollTriggers.forEach((el) => {
    const type = el.getAttribute("data-gsap-scroll-trigger-type") || "not-set";
    const settings = {
      ease: "none",
      scrollTrigger: {
        trigger: el,
        scrub: true,
        pin: false,
        start: `top center+=100px`,
        invalidateOnRefresh: true,
        markers: false,
      },
    };

    switch (type) {
      case "border": {
        settings.borderRadius = 40;
        settings.scrollTrigger.end = "+=20%";
        break;
      }
    }

    gsap.to(el, settings);
  });
}

/**
 * Scroll to hash.
 *
 * Scroll to a specific hash with a delay.
 *
 * @param {Object} smoother - The smoother object.
 * @param {String} hash - The hash to scroll to.
 * @param {Number} delaySeconds - The delay in seconds.
 */
function scrollToHash(smoother = false, hash = "", delaySeconds = 0) {
  if (hash) {
    const scrollOffset = getHeaderHeight() + 50;
    const scrollTargetOffset = smoother.offset(hash, "top top");
    const scrollToTarget = scrollTargetOffset - scrollOffset;

    gsap.to(smoother, {
      scrollTop: scrollToTarget,
      duration: 2,
      ease: "power3.out",
      delay: delaySeconds,
      onComplete: () => {
        // ScrollTrigger.refresh();
      },
    });
  }
}

/**
 * Refresh module.
 */
function refresh() {
  if (CONFIG.debug) console.log(`[ ${CONFIG.name} v.${CONFIG.version} refreshed ]`);
  ScrollTrigger.refresh();
  if (CONFIG.debug) console.log(`[ ${CONFIG.name} v.${CONFIG.version} complete ]`);
}

/**
 * Initialize module.
 */
function init(smoother = false) {
  if (CONFIG.debug) console.log(`[ ${CONFIG.name} v.${CONFIG.version} initialized ]`);

  // Add gsap-smooth-scroll class to the document element if smoother is true
  smoother && document.documentElement.classList.add("gsap-smooth-scroll");

  // Add event listeners for scroll-to-hash,
  // scroll-to-top, toggle accordion, toggle mobile menu,
  // and animate element on scroll
  onClickScrollToHash(smoother);
  onClickScrollToTop(smoother);
  onClickToggleAccordion();
  onClickToggleMobileMenu();
  onScrollAnimateElement();

  if (CONFIG.debug) console.log(`[ ${CONFIG.name} v.${CONFIG.version} complete ]`);
}

export default {
  init,
  refresh,
  scrollToHash,
};
