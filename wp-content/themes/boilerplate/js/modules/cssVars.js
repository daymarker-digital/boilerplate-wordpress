/**
 * CSS module.
 *
 * @module css
 * @version 1.0
 */

/**
 * Config object.
 *
 * Stores module information.
 *
 * @type {Object}
 */
const CONFIG = {
  debug: true,
  name: "cssVars.js",
  version: "1.0",
};

/**
 * Target HTML elements.
 *
 * Cached collection of HTML elements to be
 * used in the module.
 *
 * @type {Object}
 */
const ELEMENTS = {
  core: document.querySelectorAll("body, footer, header, main") || [],
  html: document.documentElement,
};

/**
 * Set CSS variable from viewport.
 */
function setCSSVarFromViewportHeight() {
  ELEMENTS.html.style.setProperty(`--theme-viewport-height`, `${window.innerHeight}px`);
}

/**
 * Set CSS variable from element height.
 */
function setCSSVarFromElementHeight() {
  ELEMENTS.core.forEach(({ tagName, offsetHeight }) => {
    ELEMENTS.html.style.setProperty(`--theme-${tagName.toLowerCase()}-height`, `${offsetHeight}px`);
  });
}

/**
 * Refresh module.
 */
function refresh() {
  if (CONFIG.debug) console.log(`[ ${CONFIG.name} v.${CONFIG.version} refreshed ]`);
  setCSSVarFromViewportHeight();
  setCSSVarFromElementHeight();
  if (CONFIG.debug) console.log(`[ ${CONFIG.name} v.${CONFIG.version} complete ]`);
}

/**
 * Initialize module.
 */
function init() {
  if (CONFIG.debug) console.log(`[ ${CONFIG.name} v.${CONFIG.version} initialized ]`);
  setCSSVarFromViewportHeight();
  setCSSVarFromElementHeight();
  if (CONFIG.debug) console.log(`[ ${CONFIG.name} v.${CONFIG.version} complete ]`);
}

export default { init, refresh };
