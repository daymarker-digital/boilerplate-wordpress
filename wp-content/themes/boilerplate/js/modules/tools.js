export function cleanString(input) {
  return input
    .replace(/\n/g, " ") // Replace newlines with spaces
    .replace(/\s+/g, " ") // Collapse multiple spaces into a single space
    .replace(/<[^>]*>/g, "") // Remove any HTML tags
    .trim(); // Trim leading/trailing spaces
}

export function debounce(func, delay) {
  let inDebounce;
  return function () {
    const context = this;
    const args = arguments;
    clearTimeout(inDebounce);
    inDebounce = setTimeout(() => func.apply(context, args), delay);
  };
}

export function getHeaderHeight() {
  const header = document.querySelector("#header");
  return header ? header.offsetHeight : 0;
}

export function getLocalStorageValueByKey($key) {
  return localStorage.getItem($key);
}

export function getTimeStamp() {
  let d = new Date();
  return d.getTime();
}

export function hasSpecialHash(hash = "") {
  return ["#management", "#board-of-directors"].includes(hash);
}

export function isMobileDevice() {
  if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    return true;
  }
  return false;
}

export function setCSSVariable(id = "", value = "") {
  if (id && value) document.documentElement.style.setProperty(`--${id}`, value);
}

export function setElementsHeightToCSSVariable() {
  let elements = [{ var_id: "theme-header-height--total", element_id: "header" }];

  elements.forEach((item) => {
    let { var_id, element_id } = item;
    let value = document.getElementById(element_id) ? document.getElementById(element_id).offsetHeight : 0;
    document.documentElement.style.setProperty(`--${var_id}`, `${value}px`);
  });
}

export function setLocalStorage($key, $value) {
  localStorage.setItem($key, $value);
}

export function throttle(func, limit) {
  let lastFunc;
  let lastRan;
  return function () {
    const context = this;
    const args = arguments;
    if (!lastRan) {
      func.apply(context, args);
      lastRan = Date.now();
    } else {
      clearTimeout(lastFunc);
      lastFunc = setTimeout(function () {
        if (Date.now() - lastRan >= limit) {
          func.apply(context, args);
          lastRan = Date.now();
        }
      }, limit - (Date.now() - lastRan));
    }
  };
}
