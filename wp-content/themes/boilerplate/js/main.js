import Animations from "./modules/animations";
import Credits from "./modules/credits";
import CSSVars from "./modules/cssVars";
import ScrollStates from "./modules/scrollStates";
import { throttle } from "./modules/tools";

/*  ==========================================================================
    Register plugins
============================================================================== */

gsap.registerPlugin(ScrollTrigger, ScrollSmoother);

/*  ==========================================================================
    Variables
============================================================================== */

const HASH = window.location.hash || "";

const SMOOTHER = ScrollSmoother.create({
  smooth: 1.25,
  onUpdate: (self) => ScrollStates.refresh(self),
});

/*  ==========================================================================
    Event listener for document "load"
============================================================================== */

document.addEventListener("DOMContentLoaded", () => {
  // Animations
  Animations.init(SMOOTHER);
  // CSS vars
  CSSVars.init();
  // Scroll states
  ScrollStates.init(SMOOTHER);
  // Site credits
  Credits.init();
});

/*  ==========================================================================
    Event listener for window "resize" | Throttled
============================================================================== */

window.addEventListener(
  "resize",
  throttle(() => {
    CSSVars.refresh();
  }, 250)
);

/*  ==========================================================================
    Event listener for window "resize"
============================================================================== */

window.addEventListener("resize", (event) => {
  console.log(`Event triggered :: ${event.type}`, event);
});

/*  ==========================================================================
    Event listener for window "load" event
============================================================================== */

window.addEventListener("load", (event) => {
  console.log(`Event triggered :: ${event.type}`, event);
  // Scroll to hash
  Animations.scrollToHash(SMOOTHER, HASH, 0.35);
  // Refresh CSS vars
  CSSVars.refresh();
});
