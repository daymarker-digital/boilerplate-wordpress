<?php

  /**
   * WP Theme constants, functions, scripts, styles and fonts.
   *
   * @package WordPress
   * @subpackage boilerplate
   */

  // Global constants
  define( "DD_THEME_VERSION", "0.1.0" );
  define( "DD_THEME_TEMPLATE_URL", get_template_directory_uri() );
  define( "DD_THEME_PATH", get_template_directory() );
  define( "DD_THEME_ASSETS_PATH", DD_THEME_PATH . "/assets/" );
  define( "DD_THEME_INC_PATH", DD_THEME_PATH . "/includes/" );
  define( "DD_THEME_BLOCKS_PATH", DD_THEME_PATH . "/blocks/" );
  define( "DD_THEME_ASSETS_URL", DD_THEME_TEMPLATE_URL . "/assets/" );

  // Required base theme files
  require_once DD_THEME_INC_PATH . "base/core.php";
  require_once DD_THEME_INC_PATH . "base/filters.php";
  require_once DD_THEME_INC_PATH . "base/tools.php";

  // Required project specific theme files
  // require_once DD_THEME_INC_PATH . "core.php";
  // require_once DD_THEME_INC_PATH . "filters.php";
  // require_once DD_THEME_INC_PATH . "fonts.php";
  // require_once DD_THEME_INC_PATH . "scripts.php";
  // require_once DD_THEME_INC_PATH . "styles.php";
  // require_once DD_THEME_INC_PATH . "acf-blocks.php";
  // require_once DD_THEME_INC_PATH . "acf-options.php";
  // require_once DD_THEME_INC_PATH . "custom-post-types.php";

  // Base setup functions and filters
  DD\Base\Core\init();
  DD\Base\Filters\init();

  // Project specific setup functions and filters
  // DD\Project\Core\init();
  // DD\Project\Filters\init();
  // DD\Project\ACFBlocks\init();
  // DD\Project\ACFOptions\init();
  // DD\Project\CustomBlockTypes\init();


  function admin_editor_styles() {

    $src = "/editor-style.css";
    $path = DD_THEME_TEMPLATE_URL . $src;
    $version = filemtime( DD_THEME_PATH . $src );

    wp_register_style( "custom-editor-styles", $path, [], $version, "all" );
    wp_enqueue_style( "custom-editor-styles" );

  }

  add_action( "enqueue_block_editor_assets", "admin_editor_styles" );
