<?php

  /**
   * The template for displaying the header.
   *
   * @package WordPress
   * @subpackage boilerplate
   */

  use function DD\Base\Tools\{enable_header, is_local};

?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">

  <?php if ( !is_local() ) : ?>
    <?php get_template_part( "partials/google-tag-manager" ); ?>
  <?php endif; ?>

  <head>
    <meta charset="<?php esc_attr(bloginfo( 'charset' )); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <?php wp_head(); ?>
  </head>

  <body <?php body_class(); ?>>

    <?php wp_body_open(); ?>

    <?php if ( !is_local() ) : ?>
      <?php get_template_part( "partials/google-tag-manager-no-script" ); ?>
    <?php endif; ?>

    <?php if ( enable_header() ) : ?>
      <?php get_template_part( "partials/drawer/mobile-menu" ); ?>
      <?php get_template_part( "partials/header" ); ?>
    <?php endif; ?>

    <main id="main" role="main" tabindex="-1">
      <div id="smooth-wrapper">
        <div id="smooth-content">
