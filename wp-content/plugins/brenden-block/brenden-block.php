<?php

  /**
  * Plugin Name: Brenden Block
  * Description: Custom block for Gutenberg
  *
  */

  function brenden_block_script_register() {
    
    $script_id = "brenden-block-script";
    $script_path = plugin_dir_url(__FILE__) . $script_id . ".js";
    $script_deps = [ "wp-blocks", "wp-i18n", "wp-editor" ];
    $script_ver = true;
    $script_in_footer = false; 
    
    wp_enqueue_scripts( $script_id, $script_path, $script_deps, $script_ver, $script_in_footer );
    
  }

  add_action( "enqueue_block_editor_assets", "brenden_block_script_register" );

?>