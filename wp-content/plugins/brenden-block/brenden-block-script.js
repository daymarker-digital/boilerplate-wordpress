wp.blocks.registerBlockType("brenden/brenden-block", {
  title: "Brenden Block Title",          // title of block that shows up in Gutenberg
  icon: "hammer",                        // name of dashicon handle minus the 'dashicon-'
  category: "design",                    // category of block Gutenberg list of blocks
  attributes: {
    companyName: { type: "string" },
    companyPhone: { type: "string" },
    companyAddress: { type: "string" },
    companyAddress2: { type: "string" },
    companyCity: { type: "string" },
    companyRegion: { type: "string" },
    companyPostal: { type: "string" },
  },
  edit: function(props) {
    return reactCodeHere.
  },
  save: function(props) {
    return null;
  }                    
});